if(NOT WIN32)
	message(FATAL "LivePP is only supported on Win32")
endif()

# Find livepp header 


# Setup compiler flags
add_compile_options(/Zi /Gy /Gw)

if(CMAKE_SIZEOF_VOID_P EQUAL 8)
else()
	add_compile_options(/hotpatch)
endif()

# Setup linker flags
set(LIVE_PP_LINKER_FLAGS "/FUNCTIONPADMIN /OPT:NOREF /OPT:NOICF /DEBUG:FULL /DEBUG:FASTLINK")
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${LIVE_PP_LINKER_FLAGS}")

find_path(LIVEPP_INCLUDE_DIR "API/LPP_API.h")

if(NOT LIVEPP_INCLUDE_DIR)
	message(FATAL_ERROR "Could not find LivePP on your system")
endif()

file(TO_CMAKE_PATH "${LIVEPP_INCLUDE_DIR}" LIVEPP_BINARY_DIR)
file(TO_NATIVE_PATH "${LIVEPP_INCLUDE_DIR}/API" LIVEPP_INCLUDE_DIR)