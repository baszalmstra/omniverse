uniform vec3 SunDir;
uniform float AtmosphereCameraHeight;
uniform float AtmosphereInnerRadius;
uniform float AtmosphereOuterRadius;
uniform float AtmosphereScaleDepth;
uniform float AtmosphereScale;
uniform float AtmosphereScaleOverScaleDepth;
uniform vec3 CameraPos;

uniform float KrESun;
uniform float KmESun;
uniform float Kr4PI;
uniform float Km4PI;
uniform vec3 InvWavelength;
uniform float AtmospherePhaseConstant;

const int step_count = 16;

//-------------------------------------------------------------------------------------------------
bool IsGoodScaleParam( float cosA )
{
    return (1.0 - cosA) < 1.3;
}

//-------------------------------------------------------------------------------------------------
float CalcMiePhase(float cosA, float g)
{
	float g2 = g*g;

	float a = 1.0 - g2;
	float b = 2.0 + g2;
	float c = 1.0 + cosA * cosA;
	float d = pow(1.0 + g2 - 2.0 * g * cosA, 1.5);

	return (3.0 / 2.0) * a / b * c / d;
}

//-------------------------------------------------------------------------------------------------
float ONAS_Scale( float cosA )
{
    float x = max( 1.0 - cosA, 0.0 );
    return
        AtmosphereScaleDepth *
            exp( -0.00287 + x*(0.459 + x*(3.83 + x*(-6.80 + x*5.25))) );
}

//-------------------------------------------------------------------------------------------------
void CalcRayFromCameraLen(
	vec3 pos,
    out vec3  outRayStart,
    out vec3  outRayDir,
	out float outRayLen)
{
	outRayStart = CameraPos;

    vec3 rayStartToPos = pos - outRayStart;

    outRayLen = length(rayStartToPos);
	outRayDir = rayStartToPos / outRayLen;
}

//-------------------------------------------------------------------------------------------------
float CalcRaySphereClosestInters(
                vec3 rayStart,
                vec3 rayDir,
                vec3 sphereC,
                float sphereRSqr )
{
    float B = 2.0 * dot( rayStart, rayDir ) - dot( rayDir, sphereC );

    vec3 rayStartToSphereC = sphereC - rayStart;

    float C = dot( rayStartToSphereC, rayStartToSphereC ) - sphereRSqr;

    float det = max( 0.0, B*B - 4.0 * C );

    return 0.5 * ( -B - sqrt( det ) );
}

//-------------------------------------------------------------------------------------------------
float CalcCamDistanceFromPlanetOrigin()
{
	return length(CameraPos);
}

//-------------------------------------------------------------------------------------------------
void RaytraceScatterGround(
	out vec3 outGroundColor,
    out vec3 outAtten,
    vec3 pos,
    vec3 rayStart,
    vec3 rayDir,
    float rayLen,
    float useOuterRadius,
	float near)
{
	// Calculate the ray's starting position, then calculate its scattering offset
	vec3 samplePointStart = rayStart + rayDir * near;
	float segmentLength = rayLen - near;

	// Initialize the scattering loop variables
	float sampleLength = segmentLength / float(step_count);
	float scaledLength = sampleLength * AtmosphereScale;
	vec3 sampleRay = rayDir * sampleLength;
	vec3 samplePoint = samplePointStart + sampleRay * 0.5;

	vec3 planetNormal = normalize(pos);

	float cameraOffset;
	float temp;
	{
		float depth = exp((AtmosphereInnerRadius - useOuterRadius) / AtmosphereScaleDepth);
		//float depth = exp(min(1,((useOuterRadius-AtmosphereInnerRadius)/(AtmosphereOuterRadius-AtmosphereInnerRadius))) / AtmosphereScaleDepth);
		float posLen = length(pos);
		float cameraAngle = 1.0;//dot(rayDir,planetNormal);
		float lightAngle = dot(SunDir, planetNormal);
		float cameraScale = ONAS_Scale(cameraAngle);
		float lightScale = ONAS_Scale(lightAngle);

		cameraOffset = depth * cameraScale;
		temp = lightScale + cameraScale;
	}

	// Now loop through the sample rays
	vec3 attenIntegr = vec3(0.0,0.0,0.0);
	for(int i = 0; i < step_count; ++i)
	{
		float height = length(samplePoint);
		float depth = exp(AtmosphereScaleOverScaleDepth * (AtmosphereInnerRadius-height));
		float scatter = depth * temp - cameraOffset;
		vec3 atten = exp( -scatter * (InvWavelength * Kr4PI + Km4PI));

		outAtten = atten;
		attenIntegr += atten * (depth * scaledLength);
		samplePoint += sampleRay;
	}	

	outGroundColor = attenIntegr * (InvWavelength * KrESun + KmESun);
}

//-------------------------------------------------------------------------------------------------
vec3 RaytraceScatterSky(
    vec3 rayStart,
    vec3 rayDir,
    float rayLen,
    float useOuterRadius,
	float near,
	float startDepth)
{
	// Calculate the ray's starting position, then calculate its scattering offset
	vec3 samplePointStart = rayStart + rayDir * near;
	float segmentLength = rayLen - near;

	float startAngle = dot(rayDir, samplePointStart) / useOuterRadius;

	if(!IsGoodScaleParam(startAngle))
	{
		return vec3(0.0, 0.0, 0.0);
	}

	float startOffset = startDepth * ONAS_Scale(startAngle);

	// Initialize the scattering loop variables
	float sampleLength = segmentLength / float(step_count);
	float scaledLength = sampleLength * AtmosphereScale;
	vec3 sampleRay = rayDir * sampleLength;
	vec3 samplePoint = samplePointStart + sampleRay * 0.5;

	// Now loop through the sample rays
	vec3 outColor = vec3(0.0, 0.0, 0.0);
	for(int i = 0; i < step_count; ++i)
	{
		float height = length(samplePoint);

		// clamp to avoid numerical explosion leading to inf/Nan by exp()
		// the 0.0001 epsilon is arbitrary, and for extra safety
		height = clamp(height, AtmosphereInnerRadius * 1.0001, AtmosphereOuterRadius);

		float depth = exp(AtmosphereScaleOverScaleDepth * (AtmosphereInnerRadius-height));
		
		float lightAngle = dot(SunDir, samplePoint) / height;

		float cameraAngle = dot(rayDir, samplePoint) / height;

		float scatter = startOffset + depth * (
			ONAS_Scale(lightAngle) - 
			ONAS_Scale(cameraAngle));

		vec3 atten = exp( -scatter * (InvWavelength * Kr4PI + Km4PI));

		outColor += atten * (depth * scaledLength);
		samplePoint += sampleRay;
	}	

	return outColor;
}

//-------------------------------------------------------------------------------------------------
void CalcMieAndRayleighForSky(
	out vec3 outMieColor,
	out vec3 outRayleighColor,
	vec3 rayStart,
	vec3 rayDir,
	float rayLen,
	float useOuterRadius,
	float near,
	float startDepth)
{
	vec3 baseColor = RaytraceScatterSky(
		rayStart,
		rayDir,
		rayLen,
		useOuterRadius,
		near,
		startDepth);

	outMieColor = baseColor * KmESun;
	outRayleighColor = baseColor * (KrESun * InvWavelength);
}

//-------------------------------------------------------------------------------------------------
void CalcColorsForGroundInside(
	out vec3 outGroundColor, 
	out vec3 outAtten, 
	vec3 pos)
{
	vec3 rayStart;
    vec3 rayDir;
    float rayLen;
	CalcRayFromCameraLen( pos, rayStart, rayDir, rayLen );

	RaytraceScatterGround(
            outGroundColor,
            outAtten,
            pos,
            rayStart,
            rayDir,
            rayLen,
            CalcCamDistanceFromPlanetOrigin(),
			0.0 );
}

//-------------------------------------------------------------------------------------------------
void CalcColorsForGroundOutside(
	out vec3 outGroundColor, 
	out vec3 outAtten, 
	vec3 pos)
{
	vec3 rayStart;
    vec3 rayDir;
    float rayLen;
	CalcRayFromCameraLen( pos, rayStart, rayDir, rayLen );

	// Calculate the closest intersection of the ray with the outer atmosphere
    // (which is the near point of the ray passing through the atmosphere)
    float near = CalcRaySphereClosestInters(
                                rayStart,
                                rayDir,
                                vec3(0.0, 0.0, 0.0),
								AtmosphereOuterRadius * AtmosphereOuterRadius );

	RaytraceScatterGround(
            outGroundColor,
            outAtten,
            pos,
            rayStart,
            rayDir,
            rayLen,
            AtmosphereOuterRadius,
			near);
}

//-------------------------------------------------------------------------------------------------
void CalcMieAndRayleighForSkyInside(
	out vec3 outMieColor,
	out vec3 outRayleighColor,
	out vec3 outPosToCam,
	vec3 pos)
{
	vec3 rayStart;
    vec3 rayDir;
    float rayLen;
	CalcRayFromCameraLen( pos, rayStart, rayDir, rayLen );

	float useOuterRadius = CalcCamDistanceFromPlanetOrigin();
	
	float near = 0.0;

	float startDepth = exp( AtmosphereScaleOverScaleDepth * (AtmosphereInnerRadius - useOuterRadius));

	CalcMieAndRayleighForSky(
		outMieColor,
		outRayleighColor,
		rayStart,
		rayDir,
		rayLen,
		useOuterRadius,
		near,
		startDepth);

	outPosToCam = rayStart - pos;
}

//-------------------------------------------------------------------------------------------------
void CalcMieAndRayleighForSkyOutside(
	out vec3 outMieColor,
	out vec3 outRayleighColor,
	out vec3 outPosToCam,
	vec3 pos)
{
	vec3 rayStart;
    vec3 rayDir;
    float rayLen;
	CalcRayFromCameraLen( pos, rayStart, rayDir, rayLen );

	float useOuterRadius = AtmosphereOuterRadius;
	

	float near = CalcRaySphereClosestInters(rayStart, rayDir,
		vec3(0.0,0.0,0.0),
		useOuterRadius*useOuterRadius);

	float startDepth = exp( -1.0 / AtmosphereScaleDepth);

	CalcMieAndRayleighForSky(
		outMieColor,
		outRayleighColor,
		rayStart,
		rayDir,
		rayLen,
		useOuterRadius,
		near,
		startDepth);

	outPosToCam = rayStart - pos;
}