#version 430 core

#include "atmosphere.glsl"

layout(location = 0) in vec4 pos;
layout(location = 1) in vec2 localTexCoords;
layout(location = 2) in vec4 texcoords;
layout(location = 3) in mat4 patchTransform;
layout(location = 7) in vec2 atlasTexCoordsOffset;
layout(location = 8) in vec2 morphConstants;

uniform mat4 ViewProjection;
uniform float CameraFar = 20000000;
uniform float CameraLogZConstant = 0.01;
uniform float HeightMapTexelSize;
uniform float AtlasPatchSize;
uniform float PatchGridSize;

layout(location = 0) out vec2 LocalTexCoords;
layout(location = 1) out vec2 AtlasTexCoordsOffset;
layout(location = 2) out vec2 TexCoord;
layout(location = 3) out float LogZ;
layout(location = 4) out vec3 GroundColor;
layout(location = 5) out vec3 Attenuation;
layout(location = 6) out float MorphFactor;

uniform sampler2D HeightMap;

float sampleHeight(vec2 texcoord)
{
	vec2 correctedTexcoord = atlasTexCoordsOffset
		+ texcoord*(AtlasPatchSize-HeightMapTexelSize)
		+ vec2(HeightMapTexelSize*0.5);
	return texture2D(HeightMap, correctedTexcoord).r;
}

void main()
{
	// Construct the patch local coordinates and transform them to camera space
	vec3 posPatch = vec3(pos.xy, sampleHeight(localTexCoords));
	vec4 posCamera = patchTransform*vec4(posPatch, 1);

	// Determine the camera distance
	float cameraDistance = length(posCamera);
	float morphFactor = max(0,min(1,(cameraDistance-morphConstants.x)/(morphConstants.y-morphConstants.x)));

	vec2 morphedLocalTexCoord = localTexCoords - fract(localTexCoords * (PatchGridSize-1) * 0.5) * 2.0 / PatchGridSize * morphFactor;
	vec2 morphedPos = mix(pos.xy, pos.zw, morphFactor);
	
	// Construct the patch local coordinates and transform them to camera space
	vec3 morphedPosPatch = vec3(morphedPos, sampleHeight(morphedLocalTexCoord));
	vec4 morphedPosCamera = patchTransform*vec4(morphedPosPatch, 1);
	
	// Project to the screen and apply logarithmic depth buffer
	// https://outerra.blogspot.com/2012/11/maximizing-depth-buffer-range-and.html
	gl_Position = ViewProjection*morphedPosCamera;
	const float FC = 1.0/log(CameraFar*CameraLogZConstant + 1);
	LogZ = log(gl_Position.w*CameraLogZConstant + 1)*FC;
    gl_Position.z = (2*LogZ - 1)*gl_Position.w;

	// Pass along texture coordinates
	TexCoord = mix(texcoords.xy, texcoords.zw, morphFactor);
	LocalTexCoords = morphedLocalTexCoord;
	AtlasTexCoordsOffset = atlasTexCoordsOffset;
	MorphFactor = morphFactor;

	// Determine the effects of atmospheric scattering
	vec3 posPlanet = CameraPos + morphedPosCamera.xyz;
	if ( length( CameraPos ) >= AtmosphereOuterRadius )
		CalcColorsForGroundOutside( GroundColor, Attenuation, posPlanet );
    else
		CalcColorsForGroundInside( GroundColor, Attenuation, posPlanet );
}