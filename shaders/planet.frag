#version 420 core
#extension GL_ARB_conservative_depth : enable

#include "atmosphere.glsl"

out vec4 color;
layout(depth_less) out float gl_FragDepth;

layout(location = 0) in vec2 LocalTexCoords;
layout(location = 1) in vec2 AtlasTexCoordsOffset;
layout(location = 2) in vec2 TexCoords;
layout(location = 3) in float LogZ;
layout(location = 4) in vec3 GroundColor;
layout(location = 5) in vec3 Attenuation;
layout(location = 6) in float MorphFactor;

uniform float NormalMapTexelSize;
uniform float AtlasPatchSize;

uniform sampler2D NormalMap;
uniform sampler2D DetailMap;
 
//-------------------------------------------------------------------------------------------------
void main()
{
	// Logarithmic depth: https://outerra.blogspot.com/2012/11/maximizing-depth-buffer-range-and.html
	gl_FragDepth = LogZ;

	vec2 normalMapTexcoord = AtlasTexCoordsOffset+LocalTexCoords*(AtlasPatchSize-NormalMapTexelSize)+vec2(NormalMapTexelSize*0.5);
	
	vec4 base = texture2D(DetailMap, vec2(TexCoords.x, 1-TexCoords.y));
	vec3 normal = normalize(textureLod(NormalMap, normalMapTexcoord, 0).xyz);

//	vec2 normalMapTexcoordMorphed = AtlasTexCoordsOffset+LocalTexCoords*(AtlasPatchSize-NormalMapTexelSize*2)+vec2(NormalMapTexelSize);
//	vec3 normal = normalize(mix(
//		textureLod(NormalMap, normalMapTexcoord, 0).xyz,
//		textureLod(NormalMap, normalMapTexcoordMorphed, 1).xyz,
//		MorphFactor));

	float nDotL = max(0, dot(normal, SunDir));

	color = vec4(GroundColor+base.rgb*nDotL*Attenuation, 1);
}