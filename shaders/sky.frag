#version 430 core

#include "atmosphere.glsl"

out vec4 Color;

layout(location = 0) in vec3 PosLocal;

void main()
{
	vec3 posPlanet = PosLocal * AtmosphereOuterRadius*1.025;
	vec3 posToCam, MieColor, RayleighColor;
	if(length(CameraPos) >= AtmosphereOuterRadius)
		CalcMieAndRayleighForSkyOutside(MieColor, RayleighColor, posToCam, posPlanet);
	else
		CalcMieAndRayleighForSkyInside(MieColor, RayleighColor, posToCam, posPlanet);

	float cosA = dot(SunDir, posToCam) / length(posToCam);

	float miePhase = CalcMiePhase(cosA, AtmospherePhaseConstant);
	vec3 outCol = RayleighColor + miePhase * MieColor;

	Color = vec4(outCol, 1);
}