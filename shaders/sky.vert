#version 430 core

#include "atmosphere.glsl"

layout(location = 0) in vec3 pos;

layout(location = 0) out vec3 PosLocal;

uniform mat4 ViewProjection;

void main()
{
	vec3 posPlanet = pos * AtmosphereOuterRadius*1.025;
	gl_Position = ViewProjection*vec4(posPlanet,1);
	PosLocal = pos;
}