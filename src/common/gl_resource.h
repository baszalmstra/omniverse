#pragma once

#include <glad/glad.h>
#include <cassert>
#include <utility>
#include <optional>
#include <string_view>

namespace internal
{
	struct GLBufferBridge
	{
		static GLuint Create();
		static void Delete(GLuint id);
	};

	struct GLShaderBridge
	{
		static GLuint Create(GLenum shaderType);
		static void Delete(GLuint id);
	};

	struct GLProgramBridge
	{
		static GLuint Create();
		static void Delete(GLuint id);
	};

	struct GLVertexArrayBridge
	{
		static GLuint Create();
		static void Delete(GLuint id);
	};

	struct GLTextureBridge
	{
		static GLuint Create();
		static void Delete(GLuint id);
	};
}

/**
 * Helper class for OpenGL resource to provide RAII capabilities.
 */
template<typename T>
class GLResourcePtr
{
public:
	GLResourcePtr() = default;
	explicit GLResourcePtr(GLuint id) : id_(id) {};
	GLResourcePtr(const GLResourcePtr& other) = delete;
	GLResourcePtr(GLResourcePtr &&other) { std::swap(id_, other.id_); };
	~GLResourcePtr() { Reset(); }

	GLResourcePtr<T>& operator =(GLResourcePtr &&other) { std::swap(id_, other.id_); return *this; };
	GLResourcePtr<T>& operator =(const GLResourcePtr& other) = delete;

	GLuint operator *() const
	{
		assert(HasValue());
		return id_;
	}

	operator bool() const { return id_ != 0; }

	void Reset()
	{
		if(id_ != 0)
		{
			T::Delete(id_);
			id_ = 0;
		}
	}

	GLuint Release()
	{
		GLuint id = id_;
		id_ = 0;
		return id;
	}

	bool HasValue() const { return id_ != 0; }

	void swap(GLResourcePtr& other) noexcept
	{
		std::swap(id_, other.id_);
	}

	template<typename ... TArgs>
	static GLResourcePtr<T> Create(TArgs && ...args)
	{
		return GLResourcePtr<T>(T::Create(std::forward<TArgs>(args)...));
	}

private:
	GLuint id_ = 0; // 0 means "no-object"
};

using GLBufferPtr = GLResourcePtr<internal::GLBufferBridge>;
using GLShaderPtr = GLResourcePtr<internal::GLShaderBridge>;
using GLVertexArrayPtr = GLResourcePtr<internal::GLVertexArrayBridge>;
using GLTexturePtr = GLResourcePtr<internal::GLTextureBridge>;

class GLProgramPtr : public GLResourcePtr<internal::GLProgramBridge>
{
public:
	GLProgramPtr() = default;
	explicit GLProgramPtr(GLuint id) : GLResourcePtr<internal::GLProgramBridge>(id) {};

	std::optional<GLint> findUniform(const char *name) const;

	template<typename ... TArgs>
	static GLProgramPtr Create()
	{
		return GLProgramPtr(internal::GLProgramBridge::Create(std::forward<TArgs>(args)...));
	}
};
