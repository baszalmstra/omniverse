#pragma once

#include "preprocessor.h"

namespace shaders::impl::macro
{
	bool is_defined(const std::string& val, const PreprocessedFile& processed);
	bool is_macro(const char* textPtr, PreprocessedFile& processed);
	std::string expand(const char* textPtr, const char* &textPtrAfter, 
		const std::filesystem::path& currentFile, int currentLine, PreprocessedFile& processed);
}