#include "definition.h"
#include "skip.h"
#include "classify.h"

//-------------------------------------------------------------------------------------------------
shaders::DefinitionInfo::DefinitionInfo(const char* value) : replacement(value)
{

}

//-------------------------------------------------------------------------------------------------
shaders::DefinitionInfo::DefinitionInfo(const std::string value) : replacement(std::move(value))
{
	
}

//-------------------------------------------------------------------------------------------------
shaders::DefinitionInfo::DefinitionInfo(std::vector<std::string> parameters, 
	std::string replacement) :
	replacement(std::move(replacement)),
	parameters(std::move(parameters))
{
}

//-------------------------------------------------------------------------------------------------
shaders::Definition::Definition(const std::string & name) :
	name(name)
{
}

//-------------------------------------------------------------------------------------------------
shaders::Definition::Definition(const std::string & name, const DefinitionInfo & info) :
	name(name), info(info)
{
}

//-------------------------------------------------------------------------------------------------
shaders::Definition shaders::Definition::from_format(const std::string& str)
{
	namespace skip = impl::skip;
	namespace classify = impl::classify;

	const char* begin = skip::space(str.data());
	const char* c = begin;
	while (!classify::is_eof(c) && !classify::is_space(c) && *c != '(')
		++c;
	const char *endName = c;
	c = skip::space(c);
	if (classify::is_eof(c))
		return { begin, endName };
	if(*c == '(')
	{
		DefinitionInfo info;
		do
		{
			const char* beginParam = c = skip::space(++c);
			while (!classify::is_eof(c) && !classify::is_space(c) && *c != ',' && *c != ')')
				++c;
			const char* endParam = c;
			info.parameters.emplace_back(beginParam, endParam);
			c = skip::space(c);
		} while (!classify::is_eof(c) && *c != ')');
		c = !classify::is_eof(c) ? skip::space(++c) : c;
		info.replacement = std::string{ c, begin + str.size() };
		return Definition({ begin, endName }, info);
	}

	return Definition({ begin, endName }, std::string{ c, begin + str.size() });
}
