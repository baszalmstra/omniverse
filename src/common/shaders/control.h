#pragma once

#include "preprocessor.h"

namespace shaders::impl::control
{
	std::string line_directive(const std::filesystem::path& file, int line);
	void increment_line(int &current_line, PreprocessedFile& processed);
}
