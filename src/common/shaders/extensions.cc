#include "extensions.h"

#include <unordered_set>

namespace shaders::impl::extensions
{
	std::unordered_set<std::string> extensions;

	//------------------------------------------------------------------------------------------------
	void enable_extension(const char* extension)
	{
		extensions.emplace(extension);
	}

	//------------------------------------------------------------------------------------------------
	bool extension_available(const std::string& extension)
	{
		return extensions.find(extension) != extensions.end();
	}
}