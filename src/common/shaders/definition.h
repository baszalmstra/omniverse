#pragma once

#include <string_view>
#include <vector>
#include <string>

namespace shaders
{
	struct DefinitionInfo
	{
		DefinitionInfo() = default;
		DefinitionInfo(const std::string value);
		DefinitionInfo(const char* value);
		template<typename T, typename = decltype(std::to_string(std::declval<T>()))>
		DefinitionInfo(const T& value) : DefinitionInfo(std::to_string(value)) {};
		DefinitionInfo(std::vector<std::string> parameters, std::string replacement);

		std::string replacement;
		std::vector<std::string> parameters;
	};

	struct Definition
	{
		Definition() = default;
		Definition(const std::string& name);
		Definition(const std::string&name, const DefinitionInfo& info);
		
		static Definition from_format(const std::string& str);

		std::string name;
		DefinitionInfo info;
	};
}
