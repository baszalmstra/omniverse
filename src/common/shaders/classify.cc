#include "classify.h"
#include "skip.h"

namespace shaders::impl::classify
{
	//------------------------------------------------------------------------------------------------
	bool is_eof(const char* c)
	{
		return *c == '\0';
	}

	//------------------------------------------------------------------------------------------------
	bool is_newline(const char* c)
	{
		return *c == '\n' || *c == '\r';
	}

	//------------------------------------------------------------------------------------------------
	bool is_comment(const char* c)
	{
		return strncmp(c, "//", 2) == 0 || strncmp(c, "/*", 2) == 0;
	}

	//------------------------------------------------------------------------------------------------
	bool is_space(const char* c)
	{
		return *c == ' ' || *c == '\t';
	}

	//------------------------------------------------------------------------------------------------
	bool is_name_char(const char* c)
	{
		return isalnum(*c) || *c == '_';
	}

	//------------------------------------------------------------------------------------------------
	bool is_directive(const char* c, bool checkBefore)
	{
		return *c == '#' && (!checkBefore || is_newline(skip::space_rev(c - 1)));
	}

	//------------------------------------------------------------------------------------------------
	bool is_token_equal(const char* c, const std::string_view& token, bool checkBefore, bool checkAfter)
	{
		return (!checkBefore || !isalpha(*(c - 1))) &&
			(memcmp(c, token.data(), token.size()) == 0) &&
			(!checkAfter || !is_name_char(c + token.size()));
	}
}
