#include "control.h"

//-------------------------------------------------------------------------------------------------
std::string shaders::impl::control::line_directive(const std::filesystem::path & file, int line)
{
	return "\n#line " + std::to_string(line) + " \"" + file.filename().string() + "\"\n";
}

//-------------------------------------------------------------------------------------------------
void shaders::impl::control::increment_line(int& current_line, PreprocessedFile& processed)
{
	processed.definitions["__LINE__"] = ++current_line;
}
