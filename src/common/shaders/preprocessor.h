#pragma once

#include <cstdint>
#include <filesystem>
#include <set>
#include <map>
#include <sstream>

#include "definition.h"

namespace shaders
{
	// Refers to in-shader version declaration profile, e.g. 430 core
	enum class ShaderProfile
	{
		Core = 0,
		Compatibility
	};

	// Refers to in-shader extension declarations, e.g. #extension GL_ARB_some_extension : enable
	enum class ExtensionBehavior {
		Enable = 0,
		Require,
		Warn,
		Disable
	};

	struct PreprocessedFile
	{
		int32_t version = 0;																	// GLSL shader version in integral form (e.g. 430)
		ShaderProfile profile = ShaderProfile::Core;					// GLSL shader profile (core or compatibility)
		std::filesystem::path filePath;												// The original path of the loaded file, or the name of the file.
		std::set<std::filesystem::path> dependencies;					// All files included while loading the shader
		std::map<std::string, ExtensionBehavior> extensions;	// All explicitly enabled/required glsl extensions
		std::map<std::string, DefinitionInfo> definitions;		// All definitions which have been defined in the shader
		std::string contents;																	// The fully processed shader code string
		std::vector<std::string> errors;											// Errors in the source files

		bool valid() const noexcept;
		operator bool() const noexcept;
	};

	/**
	 * @brief Loads and processes a shader file 
	 * @param filePath The source file to load
	 * @param includeDirectories A list of include directories to search in when parsing includes.
	 * @param definitions A list of predefined definitions
	 */
	PreprocessedFile preprocess_file(const std::filesystem::path& filePath,
		const std::vector<std::filesystem::path>& includeDirectories = {},
		const std::vector<Definition>& definitions = {});

	/**
	 * @brief Processes the given shader soruce and treats it as if it were a file with the given 
	 *	name.
	 * @param source The shader source
	 * @param name The name of the source which will be shown when printing errors.
	 * @param includeDirectories A list of include directories to search in when parsing includes.
	 * @param definitions A list of predefined definitions
	 */
	PreprocessedFile preprocess_source(const std::string& source, const std::string& name,
		const std::vector<std::filesystem::path>& includeDirectories = {},
		const std::vector<Definition>& definitions = {});

}
