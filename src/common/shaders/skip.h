#pragma once

#include "preprocessor.h"

namespace shaders::impl::skip
{
	const char* space(const char* c);
	const char* space_rev(const char* c);
	const char* to_next_space(const char* c);
	const char* to_next_space(const char* c, char alt);
	const char* to_endline(const char* c);
	const char* to_next_token(const char* c);
	const char* over_comments(const char* c, const std::filesystem::path& file, int& line,
		PreprocessedFile& processed, std::stringstream& result);
}
