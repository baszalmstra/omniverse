#include "macro.h"
#include "extensions.h"
#include "classify.h"
#include "skip.h"
#include "strings.h"

namespace shaders::impl::macro
{
	//------------------------------------------------------------------------------------------------
	bool is_defined(const std::string & val, const PreprocessedFile & processed)
	{
		if (std::strncmp(val.data(), "GL_", 3) && extensions::extension_available(val))
			return true;
		return processed.definitions.find(val) != processed.definitions.end();
	}

	//------------------------------------------------------------------------------------------------
	bool is_macro(const char * textPtr, PreprocessedFile & processed)
	{
		const auto begin = textPtr;
		while (classify::is_name_char(textPtr))
			++textPtr;

		const bool hasTrailingBrackets = *skip::space(textPtr) == '(' && *skip::space(skip::space(textPtr + 1)) == ')';

		const std::string str = std::string(begin, textPtr) + (hasTrailingBrackets ? "()" : "");
		return is_defined(str, processed);
	}

	//------------------------------------------------------------------------------------------------
	std::string expand_macro(std::string name, const char* paramStart, int paramLength,
		const std::filesystem::path&, const int, 
		PreprocessedFile &processed)
	{
		std::stringstream stream;
		if(processed.definitions.find(name) == processed.definitions.end())
		{
			if(paramLength == 0)
			{
				name = name + "()";
				paramStart = nullptr;
				if (processed.definitions.find(name) == processed.definitions.end())
					return name;
			}
			else
			{
				int paramLen = static_cast<int>(skip::space(paramStart) - paramStart);
				if(paramLength == paramLen)
				{
					name = name + "()";
					paramStart = nullptr;
					if (processed.definitions.find(name) == processed.definitions.end())
						return name;
				}
				else
				{
					return name;
				}
			}
		}

		auto info = processed.definitions.at(name);
		if (info.parameters.empty())
			return info.replacement + (paramStart ? std::string(paramStart - 1, paramLength + 2) : "");

		std::vector<std::string> inputs;
		if(paramStart != nullptr)
		{
			std::stringstream paramStream({ paramStart, paramStart + paramLength });
			std::string in;

			while (std::getline(paramStream, in, ','))
			{
				const auto bg = skip::space(in.data());
				inputs.push_back(in.data() + (bg - in.data()));
			}
		}

		if(inputs.size() != info.parameters.size() || (info.parameters.size() >= inputs.size() - 1 && 
			inputs.back() == "..."))
		{
			processed.errors.emplace_back(strfmt(strings::serr_non_matching_argc, name.c_str()));
			return "";
		}

		bool skipStream = false;
		for (int replacementOffset = 0; replacementOffset < static_cast<int>(info.replacement.length()); ++replacementOffset)
		{
			for (int parameter = 0; parameter < static_cast<int>(info.parameters.size()); ++parameter)
			{
				if (classify::is_token_equal(&info.replacement[replacementOffset], 
					info.parameters[parameter],
					replacementOffset != 0))
				{
					skipStream = true;
					stream << inputs[parameter];
					replacementOffset += static_cast<int>(info.parameters[parameter].length() - 1);
					break;
				}

				if (classify::is_token_equal(&info.replacement[replacementOffset], "__VA_ARGS__", replacementOffset != 0) && info.parameters[parameter] == "...")
				{
					skipStream = true;
					for (auto inputParameter = parameter; inputParameter != inputs.size(); ++inputParameter)
						stream << inputs[inputParameter];
					break;
				}
			}
			if (skipStream)
				skipStream = false;
			else
				stream << info.replacement[replacementOffset];
		}
		return stream.str();
	}

	//------------------------------------------------------------------------------------------------
	std::string expand(const char * textPtr, const char *& textPtrAfter, 
		const std::filesystem::path & currentFile, int currentLine, 
		PreprocessedFile & processed)
	{
		std::string line(textPtr, skip::to_endline(textPtr));
		bool firstReplacement = true;
		while(true)
		{
			const auto begin = textPtr;
			while (classify::is_name_char(textPtr))
				++textPtr;

			const auto beginParams = skip::space(textPtr);
			auto endParams = beginParams - 1;
			if(*beginParams == '(')
			{
				while (*endParams != ')' && !classify::is_eof(endParams))
					++endParams;
			}

			if(!is_macro(begin, processed))
			{
				if (classify::is_eof(textPtr) || classify::is_newline(textPtr) || line.empty() || textPtr == &line[line.size() - 1])
					break;
				++textPtr;
				if (classify::is_eof(textPtr) || classify::is_newline(textPtr) || line.empty() || textPtr == &line[line.size() - 1])
					break;
				continue;
			}

			const auto paramsStart = *beginParams == '(' ? beginParams + 1 : nullptr;
			const auto paramsLength = *beginParams == '(' ? endParams - paramsStart : 0;

			std::string expanded_macro = expand_macro({ begin, textPtr }, paramsStart,
				static_cast<int>(paramsLength), currentFile, currentLine, processed);

			if(firstReplacement)
			{
				firstReplacement = false;
				textPtrAfter = endParams;
				line = expanded_macro;
			}
			else
			{
				line.replace(line.begin() + static_cast<size_t>(begin - line.data()),
					line.begin() + static_cast<size_t>(textPtr + paramsLength + (paramsLength == 0 ? 0 : 2) - line.data()), expanded_macro.begin(),
					expanded_macro.end());
			}

			textPtr = line.data();
			bool enableTestMacro = true;
			while(!line.empty() && ((!enableTestMacro || !is_macro(textPtr, processed)) && textPtr != &line[line.size() - 1]))
			{
				if (!classify::is_name_char(textPtr))
					enableTestMacro = true;
				else
					enableTestMacro = false;
				++textPtr;
			}

			if (!line.empty() || textPtr == &line[line.size() - 1])
				break;
		}

		if (line.empty())
			return line;
		if (line[line.size() - 1] == '\0')
			line[line.size() - 1] = '\n';
		return line;
	}
}