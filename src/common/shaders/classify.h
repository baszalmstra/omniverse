#pragma once

#include <string_view>

namespace shaders::impl::classify
{
	bool is_eof(const char* c);
	bool is_newline(const char* c);
	bool is_comment(const char* c);
	bool is_space(const char* c);
	bool is_name_char(const char* c);
	bool is_directive(const char* c, bool checkBefore = true);
	bool is_token_equal(const char* c, const std::string_view& token, bool checkBefore = true,
		bool checkAfter = true);
}