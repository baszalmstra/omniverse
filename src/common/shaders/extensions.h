#pragma once

#include <string>

namespace shaders::impl::extensions
{
	void enable_extension(const char* extension);
	bool extension_available(const std::string& extension);
}
