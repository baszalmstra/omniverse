#include "skip.h"
#include "classify.h"
#include "control.h"

//------------------------------------------------------------------------------------------------
const char* shaders::impl::skip::space(const char* c)
{
	while (classify::is_space(c) && !classify::is_eof(c)) ++c;
	return c;
}

//------------------------------------------------------------------------------------------------
const char* shaders::impl::skip::space_rev(const char* c)
{
	while (classify::is_space(c)) --c;
	return c;
}

//------------------------------------------------------------------------------------------------
const char* shaders::impl::skip::to_next_space(const char* c)
{
	while (!classify::is_space(c) && !classify::is_newline(c) && !classify::is_eof(c))
		++c;
	return c;
}

//------------------------------------------------------------------------------------------------
const char* shaders::impl::skip::to_next_space(const char* c, char alt)
{
	while (!classify::is_space(c) && !classify::is_newline(c) && !classify::is_eof(c) && *c != alt)
		++c;
	return c;
}

//------------------------------------------------------------------------------------------------
const char* shaders::impl::skip::to_endline(const char* c)
{
	while (!classify::is_newline(c) && !classify::is_eof(c))
		++c;
	return c;
}

//------------------------------------------------------------------------------------------------
const char* shaders::impl::skip::to_next_token(const char* c)
{
	return space(to_next_space(c));
}

//------------------------------------------------------------------------------------------------
const char* shaders::impl::skip::over_comments(const char* c, const std::filesystem::path& file, int& line,
	PreprocessedFile& processed, std::stringstream& result)
{
	if (strncmp(c, "//", 2) == 0)
		return to_endline(c+2);
	
	if(strncmp(c, "/*", 2) == 0)
	{
		c += 2;
		while (strncmp(c, "*/", 2) != 0)
		{
			if (classify::is_newline(c))
				control::increment_line(line, processed);
			++c;
		}

		c += 2;
		if (processed.version != -1)
			result << control::line_directive(file, line);
	}

	return c;
}
