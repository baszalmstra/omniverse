#include "preprocessor.h"
#include "strings.h"
#include "extensions.h"
#include "classify.h"
#include "skip.h"
#include "control.h"

#include <fstream>
#include <stack>
#include <glad/glad.h>
#include "macro.h"

namespace shaders
{
	using namespace impl;

	//------------------------------------------------------------------------------------------------
	void process_impl(const std::filesystem::path& filePath, const std::string& contents, 
		const std::vector<std::filesystem::path>& includeDirectories,
		PreprocessedFile &processed, std::set<std::filesystem::path>& uniqueIncludes,
		std::stringstream& result)
	{
		const char* textPtr = contents.data();
		std::filesystem::path currentFile = filePath;
		processed.definitions["__FILE__"] = currentFile.string();
		int currentLine = 1;
		std::string curr = currentFile.filename().string();
		std::replace(curr.begin(), curr.end(), '\\', '/');

		// There is no way you could put a macro starting from the first character of the shader.
		// Set to true if the current textPtr may point to the start of a macro name.
		bool enableMacro = true;

		while(!classify::is_eof(textPtr))
		{
			textPtr = skip::over_comments(textPtr, currentFile, currentLine, processed, result);
			if (classify::is_eof(textPtr))
				break;

			if(classify::is_newline(textPtr))
			{
				control::increment_line(currentLine, processed);
				result << "\n";
				++textPtr;
				enableMacro = true;
			}
			else if(enableMacro && macro::is_macro(textPtr, processed))
			{
				result << control::line_directive(currentFile, currentLine);
				result << macro::expand(textPtr, textPtr, currentFile, currentLine, processed);
				result << control::line_directive(currentFile, currentLine + 1);
				++textPtr;
			}
			else if(classify::is_directive(textPtr, currentLine != 1))
			{
				const auto derectiveName = skip::space(textPtr + 1);
				if(classify::is_token_equal(derectiveName, "version"))
				{
					textPtr = skip::to_next_token(derectiveName);
					processed.version = (*textPtr - '0') * 100 +
						(*(textPtr + 1) - '0') * 10 +
						(*(textPtr + 2) - '0');

					processed.definitions["__VERSION__"] = std::string{ textPtr, textPtr + 3 };

					result << "#version " << *textPtr << *(textPtr + 1) << *(textPtr + 2) << " ";
					textPtr = skip::to_next_token(textPtr);

					if(classify::is_newline(textPtr) || classify::is_token_equal(textPtr, "core"))
					{
						processed.definitions["GL_core_profile"] = 1;
						processed.profile = ShaderProfile::Core;
					}
					else if(classify::is_token_equal(textPtr, "compatibility"))
					{
						processed.definitions["GL_compatibility_profile"] = 1;
						processed.profile = ShaderProfile::Compatibility;
					}
					else
					{
						processed.errors.emplace_back(strfmt(strings::serr_unrecognized_profile,
							std::string(textPtr, skip::to_endline(textPtr)).c_str()));
						processed.definitions["GL_core_profile"] = 1;
						processed.profile = ShaderProfile::Core;
					}

					while (!classify::is_newline(textPtr))
						result << *textPtr++;

					result << control::line_directive(currentFile, currentLine);
				}
				else if(classify::is_token_equal(derectiveName, "pragma"))
				{
					textPtr = skip::to_next_token(derectiveName);
					if(classify::is_token_equal(textPtr, "once"))
					{
						uniqueIncludes.emplace(currentFile);
						textPtr = skip::to_endline(textPtr);
					}
					else
					{
						result << "#pragma ";
					}
				}
				else if(classify::is_token_equal(derectiveName, "include"))
				{
					auto includeBegin = skip::to_next_token(textPtr);
					auto includeFilename = macro::expand(includeBegin, includeBegin, currentFile, currentLine, processed);

					if((includeFilename.front() != '\"' && includeFilename.back() != '\"') && 
						(includeFilename.front() != '<' && includeFilename.back() != '>'))
					{
						processed.errors.emplace_back(strfmt(strings::serr_invalid_include));
						return;
					}

					std::filesystem::path file = { std::string(includeFilename.begin() + 1, includeFilename.end() - 1) };

					bool foundFile = false;
					for(auto &&directory : includeDirectories)
					{
						if(std::filesystem::exists(directory / file))
						{
							foundFile = true;
							file = directory / file;
						}
					}

					if(!foundFile)
					{
						file = filePath.parent_path() / file;
					}

					if(!std::filesystem::exists(file))
					{
						processed.errors.emplace_back(strfmt(strings::serr_file_not_found,
							std::string(includeFilename.begin() + 1, includeFilename.end() - 1).c_str()));
						return;
					}

					if(uniqueIncludes.find(file) == uniqueIncludes.end())
					{
						result << control::line_directive(currentFile, currentLine);
						result << control::line_directive(file, 1);
						processed.dependencies.emplace(file);

						std::ifstream rootFile(file, std::ios::in);
						std::string rootFileContents(std::istreambuf_iterator<char>{rootFile}, std::istreambuf_iterator<char>{});
						process_impl(file, rootFileContents, includeDirectories, processed, uniqueIncludes, result);
					}
					textPtr = skip::to_endline(includeBegin);
					result << control::line_directive(currentFile, currentLine);
				}
				else
				{
					if (!classify::is_name_char(textPtr))
						enableMacro = true;
					else
						enableMacro = false;
					result << *textPtr;
					++textPtr;
				}
			}
			else
			{
				if (!classify::is_name_char(textPtr))
					enableMacro = true;
				else
					enableMacro = false;
				result << *textPtr;
				++textPtr;
			}
		}
	}

	//------------------------------------------------------------------------------------------------
	PreprocessedFile preprocess_file(const std::filesystem::path & filePath,
		const std::vector<std::filesystem::path>& includeDirectories,
		const std::vector<Definition>& definitions)
	{
		if (!std::filesystem::exists(filePath))
		{
			PreprocessedFile processed;
			processed.errors.emplace_back(strfmt(strings::serr_file_not_found, filePath.string().c_str()));
			return processed;
		}

		std::ifstream rootFile(filePath, std::ios::in);
		std::string contents(std::istreambuf_iterator<char>{rootFile}, std::istreambuf_iterator<char>{});
		return preprocess_source(contents, filePath.string(), includeDirectories, definitions);
	}

	//------------------------------------------------------------------------------------------------
	PreprocessedFile preprocess_source(const std::string & source, const std::string & name, 
		const std::vector<std::filesystem::path>& includeDirectories,
		const std::vector<Definition>& definitions)
	{
		static bool extensionInitialized = false;
		if(!extensionInitialized)
		{
			extensionInitialized = true;
			int n;
			glGetIntegerv(GL_NUM_EXTENSIONS, &n);
			for (auto i = 0; i < n; ++i)
				extensions::enable_extension(reinterpret_cast<const char*>(glGetStringi(GL_EXTENSIONS, i)));
		}

		PreprocessedFile processed;
		processed.version = -1;
		processed.filePath = name;

		for (auto&& definition : definitions)
			processed.definitions[definition.name] = definition.info;

		std::stringstream result;
		std::set<std::filesystem::path> uniqueIncludes;
		uniqueIncludes.emplace(name);
		process_impl(name, source, includeDirectories, processed, uniqueIncludes, result);

		processed.contents = result.str();
		return processed;
	}
}
