#pragma once

#include "gl_resource.h"
#include <string_view>
#include <filesystem>

namespace shaders
{
  GLProgramPtr program_from_files(const std::string_view& vertexShaderPath, const std::string_view& fragmentShaderPath);
}
