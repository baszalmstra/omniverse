#pragma once

#include "gl_resource.h"
#include <string_view>

namespace textures
{
	GLTexturePtr load(const std::string_view& path, std::ostream* errors = nullptr);
}
