#include "gl_resource.h"

//-------------------------------------------------------------------------------------------------
GLuint internal::GLBufferBridge::Create()
{
	GLuint id;
	glGenBuffers(1, &id);
	return id;
}

//-------------------------------------------------------------------------------------------------
void internal::GLBufferBridge::Delete(GLuint id)
{
	glDeleteBuffers(1, &id);
}

//-------------------------------------------------------------------------------------------------
GLuint internal::GLShaderBridge::Create(GLenum shaderType)
{
	assert(
		shaderType == GL_VERTEX_SHADER ||
		shaderType == GL_FRAGMENT_SHADER);
	return glCreateShader(shaderType);
}

//-------------------------------------------------------------------------------------------------
void internal::GLShaderBridge::Delete(GLuint id)
{
	return glDeleteShader(id);
}

//-------------------------------------------------------------------------------------------------
GLuint internal::GLProgramBridge::Create()
{
	return glCreateProgram();
}

//-------------------------------------------------------------------------------------------------
void internal::GLProgramBridge::Delete(GLuint id)
{
	return glDeleteProgram(id);
}

//-------------------------------------------------------------------------------------------------
GLuint internal::GLVertexArrayBridge::Create()
{
	GLuint id;
	glGenVertexArrays(1, &id);
	return id;
}

//-------------------------------------------------------------------------------------------------
void internal::GLVertexArrayBridge::Delete(GLuint id)
{
	glDeleteVertexArrays(1, &id);
}

//-------------------------------------------------------------------------------------------------
GLuint internal::GLTextureBridge::Create()
{
	GLuint id;
	glGenTextures(1, &id);
	return id;
}

//-------------------------------------------------------------------------------------------------
void internal::GLTextureBridge::Delete(GLuint id)
{
	glDeleteTextures(1, &id);
}

//-------------------------------------------------------------------------------------------------
std::optional<GLint> GLProgramPtr::findUniform(const char* name) const
{
	GLint result = glGetUniformLocation(**this, name);
	return result < 0 ? 
		std::optional<GLint>() : 
		std::optional<GLint>(result);
}
