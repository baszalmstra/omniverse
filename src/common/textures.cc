#include "textures.h"

#include <gli/gli.hpp>
#include <glm/glm.hpp>
#include <glad/glad.h>
#include "io.h"
#include <iostream>

//-------------------------------------------------------------------------------------------------
GLTexturePtr textures::load(const std::string_view& path, std::ostream* errorsPtr)
{
	std::ostream &errors = errorsPtr ? *errorsPtr : std::cerr;

	// Try to read the texture content
	auto data = io::read_file_contents(path, &errors);
	if(!data.has_value())
		return GLTexturePtr();

	// Try to interpret the contents of the texture
	auto texture = gli::load(reinterpret_cast<const char*>(data->data()), data->size());
	if(texture.empty())
	{
		errors << "'" << path << "' does not appear to be a texture that can be loaded.";
		return GLTexturePtr();
	}

	gli::gl GL(gli::gl::PROFILE_GL33);
	gli::gl::format const format = GL.translate(texture.format(), texture.swizzles());
	GLenum target = GL.translate(texture.target());

	GLTexturePtr textureId = GLTexturePtr::Create();
	glBindTexture(target, *textureId);
	glTexParameteri(target, GL_TEXTURE_BASE_LEVEL, 0);
	glTexParameteri(target, GL_TEXTURE_MAX_LEVEL, static_cast<GLint>(texture.levels() - 1));
	glTexParameteri(target, GL_TEXTURE_SWIZZLE_R, format.Swizzles[0]);
	glTexParameteri(target, GL_TEXTURE_SWIZZLE_G, format.Swizzles[1]);
	glTexParameteri(target, GL_TEXTURE_SWIZZLE_B, format.Swizzles[2]);
	glTexParameteri(target, GL_TEXTURE_SWIZZLE_A, format.Swizzles[3]);

	glm::tvec3<GLsizei> const Extent(texture.extent());
	GLsizei const FaceTotal = static_cast<GLsizei>(texture.layers() * texture.faces());

	switch(texture.target())
	{
	case gli::TARGET_1D:
		glTexStorage1D(
			target, static_cast<GLint>(texture.levels()), format.Internal, Extent.x);
		break;
	case gli::TARGET_1D_ARRAY:
	case gli::TARGET_2D:
	case gli::TARGET_CUBE:
		glTexStorage2D(
			target, static_cast<GLint>(texture.levels()), format.Internal,
			Extent.x, texture.target() == gli::TARGET_2D ? Extent.y : FaceTotal);
		break;
	case gli::TARGET_2D_ARRAY:
	case gli::TARGET_3D:
	case gli::TARGET_CUBE_ARRAY:
		glTexStorage3D(
			target, static_cast<GLint>(texture.levels()), format.Internal,
			Extent.x, Extent.y,
			texture.target() == gli::TARGET_3D ? Extent.z : FaceTotal);
		break;
	default:
		assert(0);
		break;
	}

	for(std::size_t layer = 0; layer < texture.layers(); ++layer)
		for(std::size_t face = 0; face < texture.faces(); ++face)
			for(std::size_t level = 0; level < texture.levels(); ++level)
			{
				GLsizei const layerGl = static_cast<GLsizei>(layer);
				glm::tvec3<GLsizei> extent(texture.extent(level));
				target = gli::is_target_cube(texture.target())
					? static_cast<GLenum>(GL_TEXTURE_CUBE_MAP_POSITIVE_X + face)
					: target;

				switch(texture.target())
				{
				case gli::TARGET_1D:
					if(gli::is_compressed(texture.format()))
						glCompressedTexSubImage1D(
							target, static_cast<GLint>(level), 0, extent.x,
							format.Internal, static_cast<GLsizei>(texture.size(level)),
							texture.data(layer, face, level));
					else
						glTexSubImage1D(
							target, static_cast<GLint>(level), 0, extent.x,
							format.External, format.Type,
							texture.data(layer, face, level));
					break;
				case gli::TARGET_1D_ARRAY:
				case gli::TARGET_2D:
				case gli::TARGET_CUBE:
					if(gli::is_compressed(texture.format()))
						glCompressedTexSubImage2D(
							target, static_cast<GLint>(level),
							0, 0,
							extent.x,
							texture.target() == gli::TARGET_1D_ARRAY ? layerGl : extent.y,
							format.Internal, static_cast<GLsizei>(texture.size(level)),
							texture.data(layer, face, level));
					else
						glTexSubImage2D(
							target, static_cast<GLint>(level),
							0, 0,
							extent.x,
							texture.target() == gli::TARGET_1D_ARRAY ? layerGl : extent.y,
							format.External, format.Type,
							texture.data(layer, face, level));
					break;
				case gli::TARGET_2D_ARRAY:
				case gli::TARGET_3D:
				case gli::TARGET_CUBE_ARRAY:
					if(gli::is_compressed(texture.format()))
						glCompressedTexSubImage3D(
							target, static_cast<GLint>(level),
							0, 0, 0,
							extent.x, extent.y,
							texture.target() == gli::TARGET_3D ? extent.z : layerGl,
							format.Internal, static_cast<GLsizei>(texture.size(level)),
							texture.data(layer, face, level));
					else
						glTexSubImage3D(
							target, static_cast<GLint>(level),
							0, 0, 0,
							extent.x, extent.y,
							texture.target() == gli::TARGET_3D ? extent.z : layerGl,
							format.External, format.Type,
							texture.data(layer, face, level));
					break;
				default: assert(0); break;
				}
			}

	return textureId;
}
