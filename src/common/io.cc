#include "io.h"

#include <fstream>

//-------------------------------------------------------------------------------------------------
std::optional<std::string> io::read_file_contents_as_string(const std::string_view& path, std::ostream* errors)
{
	std::ifstream stream(std::string(path), std::ios::in);
	if (stream.is_open())
	{
		std::stringstream outputStream;
		outputStream << stream.rdbuf();
		return outputStream.str();
	}
	else if (errors != nullptr)
	{
		*errors << "Unable to open '" << path << "'." << std::endl;
	}

	return std::optional<std::string>();
}

//-------------------------------------------------------------------------------------------------
std::optional<std::vector<uint8_t>> io::read_file_contents(const std::string_view & path, std::ostream * errors)
{
	std::ifstream stream(std::string(path), std::ios::in|std::ios::binary);
	if (stream.is_open())
	{
		std::vector<uint8_t> data;
		stream.seekg(0, std::ios::end);
		const size_t fileSize = stream.tellg();
		stream.seekg(0, std::ios::beg);
		data.resize(fileSize);
		stream.read(reinterpret_cast<char*>(data.data()), fileSize);
		return std::make_optional(std::move(data));
	}
	else if (errors != nullptr)
	{
		*errors << "Unable to open '" << path << "'." << std::endl;
	}

	return std::optional<std::vector<uint8_t>>();
}
