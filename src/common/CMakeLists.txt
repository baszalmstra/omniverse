add_library(common
	gl_resource.cc
	gl_resource.h
	io.h
	io.cc

    shaders.h
	shaders.cc
	shaders/definition.cc
	shaders/definition.h
	shaders/classify.h
	shaders/classify.cc
	shaders/preprocessor.cc
	shaders/preprocessor.h
	shaders/skip.h
	shaders/skip.cc
	shaders/control.h
	shaders/control.cc
	shaders/strings.h
	shaders/extensions.cc
	shaders/extensions.h
	shaders/macro.h
	shaders/macro.cc

	textures.cc
	textures.h
	)

target_link_libraries(common glad gli glm)