#pragma once

#include <optional>
#include <string>
#include <string_view>
#include <sstream>
#include <vector>

namespace io
{
	/**
	 * Reads the contents of the file at the specifies string and returns it as a string.
	 */
	std::optional<std::string> read_file_contents_as_string(const std::string_view& path, std::ostream* errors = nullptr);

	/**
	 * Reads the contents of the file at the specifies string and returns it as binary data.
	 */
	std::optional<std::vector<uint8_t>> read_file_contents(const std::string_view& path, std::ostream* errors = nullptr);
}
