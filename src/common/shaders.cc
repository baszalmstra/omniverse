#include <glad/glad.h>

#include "shaders.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <optional>
#include "io.h"
#include "shaders/preprocessor.h"

namespace
{
	void indent_write(std::stringstream& destination, const std::string_view &input, const std::string_view& indentation = "  ")
	{
		bool newLine = true;
		for(const auto& c : input)
		{
			if(c == '\n')
			{
				newLine = true;
				destination << std::endl;
			}
			else if(c == '\r')
			{

			}
			else
			{
				if(newLine)
				{
					destination << indentation;
					newLine = false;
				}
				destination << c;
			}
		}
	}

	// Helper function to return a string representation of the shader type
	const char* shader_type_to_string(GLenum shaderType)
	{
		switch(shaderType)
		{
		case GL_VERTEX_SHADER: return "vertex";
		case GL_FRAGMENT_SHADER: return "fragment";
		default: return "undefined";
		}
	}

	// Helper function to compile a shader from source
	GLShaderPtr compile_shader(GLenum shaderType, const std::string_view& path, std::stringstream* errors = nullptr)
	{
		// Preprocess the contents of the file
		auto processedFile = shaders::preprocess_file(path);
		if(!processedFile.errors.empty())
		{
			*errors << "Error compiling shader '" << path << "':" << std::endl;
			for (const auto& err : processedFile.errors)
				*errors << "\t" << err << std::endl;
			return GLShaderPtr();
		}

		// Create the shader object
		GLShaderPtr shader = GLShaderPtr::Create(shaderType);

		// Compile the shader from source
		const char* sourcePtr = processedFile.contents.data();
		glShaderSource(*shader, 1, &sourcePtr, nullptr);
		glCompileShader(*shader);

		// Get the results
		GLint result = GL_FALSE;
		int infoLogLength;
		glGetShaderiv(*shader, GL_COMPILE_STATUS, &result);
		glGetShaderiv(*shader, GL_INFO_LOG_LENGTH, &infoLogLength);

		// Check the result
		if(infoLogLength > 0 )
		{
			std::vector<char> errorMessage(infoLogLength+1);
			glGetShaderInfoLog(*shader, infoLogLength, nullptr, errorMessage.data());
			if(errors != nullptr)
			{
				*errors << "Error compiling shader '" << path << "':" << std::endl;
				indent_write(*errors, std::string(errorMessage.data(), errorMessage.size()));
			}
			shader.Reset();
		}

		// Return the result
		return shader;
	}
}

//-------------------------------------------------------------------------------------------------
GLProgramPtr shaders::program_from_files(const std::string_view & vertexShaderPath,
										 const std::string_view & fragmentShaderPath)
{
	std::stringstream errors;

	// Compile the individual shaders
	auto vertexShader = compile_shader(GL_VERTEX_SHADER, vertexShaderPath, &errors);
	auto fragmentShader = compile_shader(GL_FRAGMENT_SHADER, fragmentShaderPath, &errors);

	// Link them into a program
	GLProgramPtr program;
	if(vertexShader && fragmentShader)
	{
		program = GLProgramPtr::Create();
		glAttachShader(*program, *vertexShader);
		glAttachShader(*program, *fragmentShader);
		glLinkProgram(*program);

		// Check the result
		GLint result, infoLogLength;
		glGetProgramiv(*program, GL_LINK_STATUS, &result);
		glGetProgramiv(*program, GL_INFO_LOG_LENGTH, &infoLogLength);

		// Errors?
		if(infoLogLength > 0)
		{
			std::vector<char> errorMessage(infoLogLength+1);
			glGetProgramInfoLog(*program, infoLogLength, nullptr, errorMessage.data());
			errors << "Error linking shader program:" << std::endl;
			indent_write(errors, std::string(errorMessage.data(), errorMessage.size()));
		}

		// Cleanup
		glDetachShader(*program, *vertexShader);
		glDetachShader(*program, *fragmentShader);
		if(infoLogLength > 0) program.Reset();
	}

	// Errors?
	if(errors.tellp() > 0)
		std::cerr << errors.rdbuf();

	return program;
}
