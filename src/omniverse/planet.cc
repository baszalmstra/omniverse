#include "planet.h"
#include <common/shaders.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/gtc/matrix_transform.hpp>
#include "transform.h"
#include "camera.h"

#include <iostream>
#include "common/textures.h"
#include <vector>
#include "planet_quad_tree.h"
#include "planet_face.h"
#include "planet_generator.h"
#include "planet_patch_buffer.h"
#include "mesh/ico_sphere.h"
#include "planet_atmosphere_renderer.h"
#include "atmosphere/shader.h"

#define BUFFER_OFFSET(i) ((char *)NULL + (i))

struct PatchDrawCommand
{
	GLuint  count;
  GLuint  instanceCount;
  GLuint  firstIndex;
  GLuint  baseVertex;
  GLuint  baseInstance;
	glm::mat4 patchCamera;
	glm::vec2 atlasOffset;
	glm::vec2 morphRange;
};

//-------------------------------------------------------------------------------------------------
constexpr std::tuple<uint32_t, uint32_t> nodePartToIndexOffsetAndCount(VisibleNode::NodePart part)
{
	const uint32_t indexCount = (PlanetQuadTreeNode::VerticesPerPatch-1)*(PlanetQuadTreeNode::VerticesPerPatch-1)*6;
	switch(part)
	{
	case VisibleNode::TopLeft:
		return std::make_tuple(0, indexCount/4);
	case VisibleNode::TopRight:
		return std::make_tuple(indexCount/4, indexCount/4);
	case VisibleNode::BottomLeft:
		return std::make_tuple(indexCount/4*2, indexCount/4);
	case VisibleNode::BottomRight:
		return std::make_tuple(indexCount/4*3, indexCount/4);
	case VisibleNode::Whole:
	default:
		return std::make_tuple(0, indexCount);
	}
}

//-------------------------------------------------------------------------------------------------
Planet::Planet(PlanetPatchBuffer &patchBuffer, const glm::vec3 &pos, double radius) :
	patchBuffer_(patchBuffer),
	radius_(radius),
	patchTransformationBuffer_(GL_ARRAY_BUFFER, sizeof(glm::mat4)*16384)
{
	transform_.setTranslation(pos);

	atmosphere_.height = static_cast<float>(radius * 0.025);
}

//-------------------------------------------------------------------------------------------------
Planet::~Planet()
{
}

//-------------------------------------------------------------------------------------------------
void Planet::createIndexBuffer()
{
	// Create an index buffer to use for all patches
	nodeIndexBuffer_ = GLBufferPtr::Create();

	// Generate indices
	const size_t VertexCount = PlanetQuadTreeNode::VerticesPerPatch;
	std::vector<uint16_t> indices;
	indices.resize((VertexCount-1)*(VertexCount-1)*6);
	uint32_t offset = 0;

	auto addRegion = [&](uint32_t xStart, uint32_t yStart, uint32_t xEnd, uint32_t yEnd) {
		for(size_t y = yStart; y < yEnd-1; ++y)
			for(size_t x = xStart; x < xEnd-1; ++x)
			{
				indices[offset + 0] = static_cast<uint16_t>(x + y*VertexCount);
				indices[offset + 1] = static_cast<uint16_t>((x+1) + (y+1)*VertexCount);
				indices[offset + 2] = static_cast<uint16_t>(x + (y+1)*VertexCount);

				indices[offset + 3] = static_cast<uint16_t>((x+1) + (y+1)*VertexCount);
				indices[offset + 4] = static_cast<uint16_t>(x + y*VertexCount);
				indices[offset + 5] = static_cast<uint16_t>((x+1) + y*VertexCount);
				offset += 6;
			}
	};

	addRegion(0, 0, VertexCount / 2+1, VertexCount/2+1);
	addRegion(VertexCount/2, 0, VertexCount, VertexCount/2+1);
	addRegion(0, VertexCount/2, VertexCount/2+1, VertexCount);
	addRegion(VertexCount/2, VertexCount/2, VertexCount, VertexCount);

	assert(offset == indices.size());

	// Store the data
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *nodeIndexBuffer_);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint16_t) * indices.size(), indices.data(), GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

//-------------------------------------------------------------------------------------------------
bool Planet::init()
{
	atmosphereRenderer_ = std::make_unique<PlanetAtmosphereRenderer>(*this);
	if(!atmosphereRenderer_->init())
		return false;

	// Create planet generator
	generator_ = std::make_unique<PlanetGenerator>(*this);

	processor_ = std::make_unique<PatchGenerationProcessor>(*this, std::min(std::thread::hardware_concurrency()-1, 4u)); // Bas: I'm limiting this to 4 because my mouse freezes if it hits high CPU%....

	// Create patch geometry
	faces_ = std::unique_ptr<std::array<PlanetQuadTree, 6>>(new std::array<PlanetQuadTree, 6> {
			PlanetQuadTree(*this, *processor_, PlanetFace::Right),
			PlanetQuadTree(*this, *processor_, PlanetFace::Left),
			PlanetQuadTree(*this, *processor_, PlanetFace::Top),
			PlanetQuadTree(*this, *processor_, PlanetFace::Bottom),
			PlanetQuadTree(*this, *processor_, PlanetFace::Front),
			PlanetQuadTree(*this, *processor_, PlanetFace::Back),
	});

	// Load heightmap texture
	detailMap_ = textures::load("images/bathroomtime2-ao.dds");

	createIndexBuffer();

	// Initialize the patch transformation storage buffer
	if(!patchTransformationBuffer_.init())
		return false;

	// Create vertex array
	vao_ = GLVertexArrayPtr::Create();
	{
		glBindVertexArray(*vao_);

		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);
		glEnableVertexAttribArray(3);
		glEnableVertexAttribArray(4);
		glEnableVertexAttribArray(5);
		glEnableVertexAttribArray(6);
		glEnableVertexAttribArray(7);
		glEnableVertexAttribArray(8);

		glVertexAttribFormat(0, 4, GL_FLOAT, GL_FALSE, 0);
		glVertexAttribFormat(1, 2, GL_FLOAT, GL_FALSE, 16);
		glVertexAttribFormat(2, 4, GL_FLOAT, GL_FALSE, 24);

		glVertexAttribFormat(3, 4, GL_FLOAT, GL_FALSE, 20);
		glVertexAttribFormat(4, 4, GL_FLOAT, GL_FALSE, 36);
		glVertexAttribFormat(5, 4, GL_FLOAT, GL_FALSE, 52);
		glVertexAttribFormat(6, 4, GL_FLOAT, GL_FALSE, 68);
		glVertexAttribFormat(7, 2, GL_FLOAT, GL_FALSE, 84);
		glVertexAttribFormat(8, 2, GL_FLOAT, GL_FALSE, 92);

		glVertexAttribBinding(0, 0);
		glVertexAttribBinding(1, 0);
		glVertexAttribBinding(2, 0);
		glVertexAttribBinding(3, 1);
		glVertexAttribBinding(4, 1);
		glVertexAttribBinding(5, 1);
		glVertexAttribBinding(6, 1);
		glVertexAttribBinding(7, 1);
		glVertexAttribBinding(8, 1);

		glVertexBindingDivisor(0, 0);
		glVertexBindingDivisor(1, 1);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *nodeIndexBuffer_);

		glBindVertexArray(0);
	}

	// Load shader
	program_ = shaders::program_from_files("shaders/planet.vert", "shaders/planet.frag");
	uniformViewProjection_ = glGetUniformLocation(*program_, "ViewProjection");
	uniformNormalMapTexelSize_ = glGetUniformLocation(*program_, "NormalMapTexelSize");
	uniformHeightMapTexelSize_ = glGetUniformLocation(*program_, "HeightMapTexelSize");
	uniformAtlasPatchSize_ = glGetUniformLocation(*program_, "AtlasPatchSize");
	uniformPatchGridSize_ = glGetUniformLocation(*program_, "PatchGridSize");
	uniformHeightMap_ = glGetUniformLocation(*program_, "HeightMap"); 
	uniformNormalMap_ = glGetUniformLocation(*program_, "NormalMap"); 
	uniformDetailMap_ = glGetUniformLocation(*program_, "DetailMap");
  atmosphereUniforms_ = atmosphere::shader::find_uniforms(program_);

	return program_;// && heightMap_;
}

//-------------------------------------------------------------------------------------------------
void Planet::drawTerrain(const Frustum& frustum, PlanetRenderStats& stats)
{
	//Transform planetCamera = frustum.transform().inverse() * transform_;
	const Transform cameraPlanet = transform().inverse() * frustum.transform();
	Frustum frustumPlanet(cameraPlanet, frustum.projection());

	const glm::mat4 projection = frustum.projection();
	const glm::mat4 view = glm::mat4_cast(frustum.transform().inverse().orientation());
	const glm::mat4 viewProjection = projection*view;

	const glm::vec3 sunDir(1,0,0);
		
	// Iterate the quad tree and record the nodes to draw
	std::deque<VisibleNode> visibleNodes;
	for(auto& face : *faces_)
	{
		face.ensurePatchesResident(frustumPlanet);
		face.queryVisibleNodes(frustumPlanet, visibleNodes);		
		// Process the generation requests that are finished
		face.processGenerationRequest();
	}

	if(visibleNodes.empty())
		return;

	// Build the draw call list
	size_t patchTransformationBufferOffset;
	const size_t patchTransformationBufferRange = visibleNodes.size()*sizeof(PatchDrawCommand);
	const auto patchTransformations = reinterpret_cast<PatchDrawCommand*>(
		patchTransformationBuffer_.map(patchTransformationBufferRange, &patchTransformationBufferOffset));
	uint32_t patchesToRender = 0;
	for(size_t i = 0; i < visibleNodes.size(); ++i)
	{
		auto &node = visibleNodes[i];
		if(!node.node->has_geometry())
			continue;

		uint32_t indexOffset, indexCount;
		std::tie(indexOffset, indexCount) = nodePartToIndexOffsetAndCount(node.part);

		auto &command = patchTransformations[patchesToRender];
		command.count = indexCount;
		command.instanceCount = 1;
		command.firstIndex = indexOffset;
		command.baseVertex = static_cast<GLuint>(patchBuffer().baseVertex(*node.node->patchId));
		command.baseInstance = static_cast<GLuint>(patchesToRender);
		command.patchCamera = node.poseCamera;
		command.atlasOffset = patchBuffer().texcoordsOffset(*node.node->patchId);
		command.morphRange = node.morphRange;
		stats.triangles += indexCount/3;
		++patchesToRender;
	}
	patchTransformationBuffer_.unmap();

	// Set state
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	glEnable(GL_DEPTH_TEST);

	// Set shader & uniforms
	glUseProgram(*program_);
	glUniformMatrix4fv(uniformViewProjection_, 1, GL_FALSE, &viewProjection[0][0]);
  atmosphere::shader::set_uniforms(atmosphereUniforms_, atmosphere(), 
    static_cast<float>(radius()), frustumPlanet.transform().translation(), sunDir);
	glUniform1f(uniformNormalMapTexelSize_, 1.0f/patchBuffer().normal_texture_atlas().size());
	glUniform1f(uniformHeightMapTexelSize_, 1.0f/patchBuffer().height_offset_texture_atlas().size());
	glUniform1f(uniformAtlasPatchSize_, patchBuffer().patchTexcoordSize());
	glUniform1f(uniformPatchGridSize_, static_cast<float>(PlanetQuadTreeNode::VerticesPerPatch));

	// Textures
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, *patchBuffer().normal_texture_atlas().texture());
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
	glUniform1i(uniformNormalMap_, 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, *patchBuffer().height_offset_texture_atlas().texture());
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glUniform1i(uniformHeightMap_, 1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, *detailMap_);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glUniform1i(uniformDetailMap_, 2);

	// Bind vertex data
	glBindVertexArray(*vao_);

	glBindVertexBuffer(0, *patchBuffer().geometry_buffer().buffer(), 0, sizeof(PlanetVertex));
	glBindVertexBuffer(1, *patchTransformationBuffer_.buffer(), patchTransformationBufferOffset, sizeof(PatchDrawCommand));

	// Setup stencil operation to mark terrain in the stencil buffer
	glEnable(GL_STENCIL_TEST);
	glStencilFunc(GL_ALWAYS, 1, 0xFF);
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
	glStencilMask(0x1);

	// Draw
	glBindBuffer(GL_DRAW_INDIRECT_BUFFER, *patchTransformationBuffer_.buffer());
	glMultiDrawElementsIndirect(GL_TRIANGLES, GL_UNSIGNED_SHORT, BUFFER_OFFSET(patchTransformationBufferOffset), patchesToRender, static_cast<GLsizei>(sizeof(PatchDrawCommand)));

	stats.drawCalls++;

	// Reset bindings
	glBindVertexArray(0);
	glDisable(GL_CULL_FACE);
	glDisable(GL_STENCIL_TEST);
}

//-------------------------------------------------------------------------------------------------
void Planet::drawAtmosphere(const Frustum& frustum, PlanetRenderStats& stats)
{
	atmosphereRenderer_->draw(frustum, stats);
}
