#pragma once

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

struct PlanetVertex
{
	glm::vec4 posAndMorphDelta;
	glm::vec2 localTexCoords;
	glm::vec4 texcoord;
};