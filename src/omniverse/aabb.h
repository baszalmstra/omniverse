#pragma once

#include <glm/vec3.hpp>
#include <limits>

struct AABB
{
  glm::dvec3 min = glm::dvec3(
    std::numeric_limits<double>::max(),
    std::numeric_limits<double>::max(),
    std::numeric_limits<double>::max());
  glm::dvec3 max = glm::dvec3(
    std::numeric_limits<double>::lowest(),
    std::numeric_limits<double>::lowest(),
    std::numeric_limits<double>::lowest());

  glm::dvec3 center() const;

  bool isValid() const;
  bool isPoint() const;

  AABB united(const glm::dvec3& p) const;
  AABB united(const AABB& p) const;

  double distance(const glm::dvec3& p) const;
  double distanceSquared(const glm::dvec3& p) const;

	glm::dvec3 closestPoint(const glm::dvec3& p) const;

	bool intersectsSphere(const glm::dvec3& p, double radius) const;

  bool contains(const glm::dvec3& p) const;

	template<typename T>
	void getCorners(glm::tvec3<T> corners[8]) const;
};

inline glm::dvec3 AABB::center() const
{
  return (min + max) * 0.5;
}

inline bool AABB::isValid() const
{
  return min.x <= max.x;
}

inline bool AABB::isPoint() const
{
  return min == max;
}

inline AABB AABB::united(const glm::dvec3& p) const
{
  return AABB { 
    glm::dvec3(std::min(p.x, min.x), std::min(p.y, min.y), std::min(p.z, min.z)),
    glm::dvec3(std::max(p.x, max.x), std::max(p.y, max.y), std::max(p.z, max.z))
  };
}

inline AABB AABB::united(const AABB& p) const
{
  return AABB{
    glm::dvec3(std::min(p.min.x, min.x), std::min(p.min.y, min.y), std::min(p.min.z, min.z)),
    glm::dvec3(std::max(p.max.x, max.x), std::max(p.max.y, max.y), std::max(p.max.z, max.z))
  };
}

inline double AABB::distance(const glm::dvec3& p) const
{
  glm::dvec3 delta(
    std::max(0.0, std::max(min.x - p.x, p.x - max.x)),
    std::max(0.0, std::max(min.y - p.y, p.y - max.y)),
    std::max(0.0, std::max(min.z - p.z, p.z - max.z))
  );
  return glm::length(delta);
}

inline double AABB::distanceSquared(const glm::dvec3& p) const
{
  glm::dvec3 delta(
    std::max(0.0, std::max(min.x - p.x, p.x - max.x)),
    std::max(0.0, std::max(min.y - p.y, p.y - max.y)),
    std::max(0.0, std::max(min.z - p.z, p.z - max.z))
  );
  return glm::dot(delta, delta);
}

inline glm::dvec3 AABB::closestPoint(const glm::dvec3& p) const
{
	return glm::dvec3(
		std::max(min.x, std::min(max.x, p.x)),
		std::max(min.y, std::min(max.y, p.y)),
		std::max(min.z, std::min(max.z, p.z))
	);
}

inline bool AABB::intersectsSphere(const glm::dvec3 & p, double radius) const
{
	return distanceSquared(p) <= radius*radius;
}

inline bool AABB::contains(const glm::dvec3& p) const
{
  return p.x >= min.x && p.x <= max.x &&
    p.y >= min.y && p.y <= max.y &&
    p.z >= min.z && p.z <= max.z;
}

template<typename T>
inline void AABB::getCorners(glm::tvec3<T> corners[8]) const
{
	corners[0] = static_cast<glm::tvec3<T>>(glm::dvec3(min.x, min.y, min.z));
	corners[1] = static_cast<glm::tvec3<T>>(glm::dvec3(max.x, min.y, min.z));
	corners[2] = static_cast<glm::tvec3<T>>(glm::dvec3(max.x, max.y, min.z));
	corners[3] = static_cast<glm::tvec3<T>>(glm::dvec3(min.x, max.y, min.z));
	corners[4] = static_cast<glm::tvec3<T>>(glm::dvec3(min.x, min.y, max.z));
	corners[5] = static_cast<glm::tvec3<T>>(glm::dvec3(max.x, min.y, max.z));
	corners[6] = static_cast<glm::tvec3<T>>(glm::dvec3(max.x, max.y, max.z));
	corners[7] = static_cast<glm::tvec3<T>>(glm::dvec3(min.x, max.y, max.z));
}
