#pragma once

#include <iostream>
#include<glm/glm.hpp>
#include<glm/gtc/quaternion.hpp>

#include <optional>

class Transform
{
public:
	constexpr Transform(
		const glm::dvec3& translation = glm::dvec3(0.f, 0.f, 0.f),
		const glm::dquat& orientation	= glm::dquat(1.f, 0.f, 0.f, 0.f),
		const glm::vec3 &scale = glm::vec3(1.f, 1.f, 1.f)) :
		translation_(translation),
		orientation_(orientation),
		scale_(scale) {}

	Transform(const Transform&) = default;
	Transform(Transform&&) = default;

	Transform& operator=(const Transform&) = default;
	Transform& operator=(Transform&&) = default;

	void setOrientation(const glm::dquat& orientation);
	const glm::dquat& orientation() const { return orientation_; }

	void setTranslation(const glm::dvec3& position);
	void setTranslation(double x, double y, double z) { setTranslation(glm::dvec3(x,y,z)); }
	const glm::dvec3& translation() const { return translation_; }

	void setScale(const glm::dvec3& scale);
	void setScale(double scale) { setScale(glm::dvec3(scale)); }
	void setScale(double x, double y, double z) { setScale(glm::dvec3(x,y,z)); }
	const glm::dvec3& scale() const { return scale_;}

	glm::dvec3 xAxis() const { return glm::dvec3(1.f, 0, 0) * glm::dquat(orientation_); }
	glm::dvec3 yAxis() const { return glm::dvec3(0, 1.f, 0) * glm::dquat(orientation_); }
	glm::dvec3 zAxis() const { return glm::dvec3(0, 0, 1.f) * glm::dquat(orientation_); }

	void rotateLocal(const glm::dvec3& axis, double radians) { setOrientation(glm::dquat(orientation_)*glm::rotate(glm::dquat(1.0, 0.0, 0.0, 0.0), radians, axis)); }
	void pitchLocal(double radians) { rotateLocal(glm::dvec3(1.0, 0.0, 0.0), radians); }
	void yawLocal(double radians)	{ rotateLocal(glm::dvec3(0.0, 1.0, 0.0), radians); }
	void rollLocal(double radians) { rotateLocal(glm::dvec3(0.0, 0.0, 1.0), radians); }

	void rotate(const glm::dvec3& axis, double radians) { setOrientation(glm::rotate(glm::dquat(1.0, 0.0, 0.0, 0.0), radians, axis)*glm::dquat(orientation_)); }
	void pitch(double radians) { rotate(glm::dvec3(1.0, 0.0, 0.0), radians); }
	void yaw(double radians) { rotate(glm::dvec3(0.0, 1.0, 0.0), radians); }
	void roll(double radians) { rotate(glm::dvec3(0.0, 0.0, 1.0), radians); }

	void translate(const glm::dvec3& distance) { setTranslation(translation_ + distance); }
	void translate(double x, double y, double z) { translate(glm::dvec3(x,y,z)); }

	void translateLocal(const glm::dvec3& distance) { setTranslation(translation_ +  glm::dquat(orientation_) * distance); }
	void translateLocal(double x, double y, double z) { translateLocal(glm::dvec3(x,y,z)); }

	glm::dvec3 operator*(const glm::dvec3& v) const
	{
		return transformPoint(v);
	}

	glm::dvec3 transformPoint(const glm::dvec3& v) const { return glm::dquat(orientation_) * v + translation_; }
	glm::vec3 transformPoint(const glm::vec3& v) const { return glm::vec3(glm::dquat(orientation_) * glm::dvec3(v) + translation_); }

	glm::dvec3 transformVector(const glm::dvec3& v) const { return glm::dquat(orientation_) * v; }
	glm::vec3 transformVector(const glm::vec3& v) const { return glm::vec3(glm::dquat(orientation_) * glm::dvec3(v)); }

	Transform operator*(const Transform& tr) const
	{
		Transform ret;
		ret.setTranslation(translation_ + glm::dquat(orientation_) * tr.translation());
		ret.setOrientation(orientation_ * tr.orientation());
		return ret;
	}

	Transform inverse() const
	{
		Transform ret;
		ret.setOrientation(glm::inverse(orientation_));
		ret.setTranslation(glm::dquat(ret.orientation()) * -translation_);
		return ret;
	}
	
	friend std::ostream& operator<< (std::ostream& out, const Transform& t)
	{
		// Sjoerd is een luie koe
		out << "[ " << t.translation().x << ", " << t.translation().y << ", " << t.translation().z << " ]";
		return out;
	}

	const glm::mat4 &transformation() const;
	const glm::mat4 &inverseTransformation() const;

private:
	glm::dvec3 translation_ = glm::dvec3(0.0, 0.0, 0.0);
	glm::dquat orientation_	= glm::dquat(1.0, 0.0, 0.0, 0.0);
	glm::dvec3 scale_				= glm::dvec3(1.0, 1.0, 1.0);

	mutable std::optional<glm::mat4> transformation_;
	mutable std::optional<glm::mat4> inverseTransformation_;
};
