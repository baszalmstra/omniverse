#pragma once

#include <cstdint>
#include <common/gl_resource.h>

/**
 * Holds a vertex and index buffer that define a planar geometric object.
 */
struct PlanetPatch
{
	PlanetPatch(uint32_t numVertices = 65);
	PlanetPatch(const PlanetPatch& other) = delete;

	GLBufferPtr vertexBuffer;
	GLBufferPtr indexBuffer;

	uint32_t indexCount;
	uint32_t vertexCount;
};
