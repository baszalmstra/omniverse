#pragma once

#include <common/gl_resource.h>
#include <vector>
#include <optional>
#include "planet_vertex.h"
#include "array_view.h"

/**
 * A vertex buffer object that stores geometry per patch.
 */
class PatchGeometryBuffer
{
public:
	PatchGeometryBuffer(size_t maxPatchCount, size_t bytesPerPatch);

	bool init();

	// Maps the memory of patch so we can write to it.
	void* map(size_t id);

	// Unmaps the memory of the patch
	void unmap(size_t id);
	
	/// Returns the buffer resource
	const GLBufferPtr& buffer() const;

private:
	size_t maxPatchCount_, bytesPerPatch_;
	GLBufferPtr buffer_;
};

/**
 * A texture object that stores square textures per patch in a single giant texture.
 */
class PatchTextureAtlas
{
public:
	PatchTextureAtlas(size_t maxPatchCount, uint32_t levels, size_t texelsPerPatch, GLenum format);

	// Initializes the backing texture
	bool init();

	/// Updates the data for a specific patch
	void write(size_t id, uint32_t level, const av::array_view<uint8_t>& data, GLenum format, GLenum type);
	
	/// Returns the buffer resource
	const GLTexturePtr& texture() const { return texture_; };
	
	/// Returns the size of the texture in pixels
	uint32_t size() const { return static_cast<uint32_t>(texelsPerPatch_*patchStride_); }

	/// Returns the number of texels per patch
	uint32_t patchSize() const { return static_cast<uint32_t>(texelsPerPatch_); }

private:
	GLTexturePtr texture_;
	std::array<GLBufferPtr, 32> transferBuffers_;
	uint32_t nextTransferBuffer_ = 0;
	GLenum format_;
  uint32_t levels_;
	size_t patchStride_{}, maxPatchCount_, texelsPerPatch_;
};

/**
 * A typed specialization of the PatchGeometryBuffer for easier access and a better understanding
 * of what is actually contained in the buffer.
 */
template<typename T>
class TypedPatchGeometryBuffer : public PatchGeometryBuffer
{
public:
	TypedPatchGeometryBuffer(size_t maxPatchCount, size_t verticesPerPatch) :
		PatchGeometryBuffer(maxPatchCount, verticesPerPatch*sizeof(T)) {}

	T *map(size_t id) { return static_cast<T*>(PatchGeometryBuffer::map(id)); }
};

template<typename T>
class TypedPatchTextureAtlas : public PatchTextureAtlas {};

template<typename T, GLenum bufferFormat, GLenum dataFormat, GLenum dataType>
class TypedPatchTextureAtlasBase : public PatchTextureAtlas
{
public:
	TypedPatchTextureAtlasBase(size_t maxPatchCount, uint32_t levels, size_t texelsPerPatch) :
		PatchTextureAtlas(maxPatchCount, levels, texelsPerPatch, bufferFormat) {};

	void write(size_t id, uint32_t level, const av::array_view<T>& data)
	{
		return PatchTextureAtlas::write(id, level,
			av::array_view<uint8_t>(reinterpret_cast<uint8_t*>(data.data()), data.size()*sizeof(T)), 
			dataFormat, dataType);
	}
};

template<> class TypedPatchTextureAtlas<glm::vec3> : public TypedPatchTextureAtlasBase<glm::vec3, GL_RGB32F, GL_RGB, GL_FLOAT> {
public:
	TypedPatchTextureAtlas(size_t maxPatchCount, uint32_t levels, size_t texelsPerPatch)
		: TypedPatchTextureAtlasBase(maxPatchCount, levels, texelsPerPatch)
	{
	}
};
template<> class TypedPatchTextureAtlas<float> : public TypedPatchTextureAtlasBase<float, GL_R32F,  GL_RED, GL_FLOAT> {
public:
	TypedPatchTextureAtlas(size_t maxPatchCount, uint32_t levels, size_t texelsPerPatch)
		: TypedPatchTextureAtlasBase(maxPatchCount, levels, texelsPerPatch)
	{
	}
};

/**
 * An object that dynamically allocates data for patches.
 */
class PlanetPatchBuffer
{
public:
	PlanetPatchBuffer(size_t maxPatchCount = 2048);

	bool init();

	// Acquires an id for a new patch
	size_t acquire();

	// Release the id of a patch so it can be reused by another patch.
	void release(size_t id);

	// Returns the total number of resident patches
	size_t residentPatchCount() const { return totalAcquired_;};

	// Returns the geometry buffer
	TypedPatchGeometryBuffer<PlanetVertex> &geometry_buffer() { return geometryBuffer_; }

	// Returns the texture atlas for normals
	TypedPatchTextureAtlas<glm::vec3> &normal_texture_atlas() { return normalTextureAtlas_;}

	// Returns the texture atlas with height offsets
	TypedPatchTextureAtlas<float> &height_offset_texture_atlas() { return heightTextureAtlas_;}

	glm::vec2 texcoordsOffset(size_t id) const
	{
		return glm::vec2(
		((id % patchStride_)) / static_cast<float>(patchStride_),
		((id / patchStride_)) / static_cast<float>(patchStride_));
	}
	
	float patchTexcoordSize() const
	{
		return 1.0f/patchStride_;
	}

	// Returns the vertex base for the given patch id
	size_t baseVertex(size_t id) const;
	
private:
	size_t availableSectionTail_, availableSectionHead_;
	std::vector<size_t> availableSections_;
	size_t totalAcquired_;
	size_t patchStride_;

	TypedPatchGeometryBuffer<PlanetVertex> geometryBuffer_;
	TypedPatchTextureAtlas<glm::vec3> normalTextureAtlas_;
	TypedPatchTextureAtlas<float> heightTextureAtlas_;
};