#include "camera_controller.h"
#include "camera.h"
#include <GLFW/glfw3.h>
#include "glm/gtx/norm.inl"
#include <algorithm>

//-------------------------------------------------------------------------------------------------
CameraController::CameraController(GLFWwindow *window, Camera & camera) : window_(window), camera_(camera)
{
}

//-------------------------------------------------------------------------------------------------
void CameraController::keyCallback(int key, int /*scancode*/, int action, int /*mods*/)
{
	if(key == GLFW_KEY_W && action == GLFW_PRESS)
		movementVector_.z = -1.0f;
	if(key == GLFW_KEY_S && action == GLFW_PRESS)
		movementVector_.z = 1.0f;
	if(key == GLFW_KEY_W && action == GLFW_RELEASE && movementVector_.z < 0.0f)
		movementVector_.z = 0.0f;
	if(key == GLFW_KEY_S && action == GLFW_RELEASE && movementVector_.z > 0.0f)
		movementVector_.z = 0.0f;
	if(key == GLFW_KEY_A && action == GLFW_PRESS)
		movementVector_.x = -1.0f;
	if(key == GLFW_KEY_D && action == GLFW_PRESS)
		movementVector_.x = 1.0f;
	if(key == GLFW_KEY_A && action == GLFW_RELEASE && movementVector_.x < 0.0f)
		movementVector_.x = 0.0f;
	if(key == GLFW_KEY_D && action == GLFW_RELEASE && movementVector_.x > 0.0f)
		movementVector_.x = 0.0f;
}

//-------------------------------------------------------------------------------------------------
void CameraController::mouseButtonCallback(int button, int action, int /*mods*/)
{
  if(button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
		setCaptureMouse(true);
  if(button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE)
		setCaptureMouse(false);
}

//-------------------------------------------------------------------------------------------------
void CameraController::mouseScrollCallback(double /*xoffset*/, double yoffset)
{
	if(yoffset < 0 && movementSpeed_ > 10)
		movementSpeed_ *= 0.5;
	else if(yoffset > 0 && movementSpeed_ < 1000000)
		movementSpeed_ *= 2;
}

//-------------------------------------------------------------------------------------------------
void CameraController::mouseMoveCallback(double x, double y)
{
	glm::dvec2 pos(x,y);
	mouseDelta_ += pos - lastMousePosition_;
	lastMousePosition_ = pos;
}

//-------------------------------------------------------------------------------------------------
void CameraController::update(float dt, const Planet& planet)
{
	Transform& transform = camera_.transform();

	double planetDistanceOld = glm::length(transform.translation() - planet.transform().translation()) - planet.radius();
	movementSpeed_ = std::min(movementSpeed_, 50 + planetDistanceOld * 10);

	if(glm::length2(movementVector_) > 0.f)
		transform.translateLocal(glm::normalize(movementVector_)*static_cast<double>(dt)*movementSpeed_);

	// Determine the up_dir based on the closest planet
	glm::dvec3 up = glm::normalize(transform.translation() - planet.transform().translation());

	// Get distance of camera to planet
	double planetDistance = glm::length(transform.translation() - planet.transform().translation()) - planet.radius();
	if(planetDistance < 2)
	{
		transform.setTranslation(planet.transform().translation() + up * (planet.radius() + 2.01));
		planetDistance = 2.0;
	}

	if(mouseCaptured_)
	{
		if(planetDistance < 0.1 * planet.radius())
			transform.rotate(up, mouseDelta_.x*-0.003f);
		else
			transform.rotateLocal(glm::dvec3(0, 1, 0), mouseDelta_.x*-0.003f);
		transform.pitchLocal(mouseDelta_.y*-0.003f);

		mouseDelta_ = glm::dvec2(0,0);
	}

	// Slowly move the up vector of the camera to the given up vector
	if(planetDistance < 0.1 * planet.radius())
	{
//		float corrSpeed = 1.0 / (std::max<float>(0, planetDistance / 100) + 1.0);
		float corrSpeed = 5;
    glm::vec3 local_up = transform.inverseTransformation() * glm::vec4(up, 0);
		transform.rollLocal(dt * -local_up.x * corrSpeed);
//		transform.pitchLocal(0.1 * dt * local_up.z * corrSpeed);
	}

//	transform.rollLocal(dt*0.1);

}

//-------------------------------------------------------------------------------------------------
void CameraController::setCaptureMouse(bool capture)
{
	if(capture == mouseCaptured_)
		return;

	mouseCaptured_ = capture;

	if(mouseCaptured_)
	{
		glfwSetInputMode(window_, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		mouseDelta_ = glm::dvec2(0,0);
	}
	else
		glfwSetInputMode(window_, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
}

//-------------------------------------------------------------------------------------------------
glm::dvec2 CameraController::mousePosition() const
{
	double x, y;
	glfwGetCursorPos(window_, &x, &y);
	return glm::vec2(static_cast<float>(x),static_cast<float>(y));
}
