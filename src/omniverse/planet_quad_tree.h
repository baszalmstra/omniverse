#pragma once

#include <array>
#include <common/gl_resource.h>
#include "planet_face.h"
#include <deque>
#include <memory>
#include <vector>
#include "aabb.h"
#include "planet_vertex.h"
#include <atomic>
#include "horizon_culling.h"

struct VisibleNode
{
	class PlanetQuadTreeNode *node;
	glm::mat4 poseCamera;
	glm::vec2 morphRange;

	enum NodePart : uint8_t
	{
		TopLeft = 0,
		TopRight = 1,
		BottomLeft = 2,
		BottomRight = 3,
		Whole = 0xf,
	} part;
};

class PlanetQuadTreeNode
{
public:
	enum
	{
		VerticesPerPatch = 32+1+4, // 32 triangles per patch dimension,
                               // +1 for the vertex that is shared with the next patch
                               // +4 for 2 vertices overlap with the next patch
		NormalsPerPatch = VerticesPerPatch*4,
	};

public:
	~PlanetQuadTreeNode();

	bool has_children() const { return children != nullptr; }
	bool is_initialized() const { return patchId.has_value(); }
	bool has_geometry() const { return patchId.has_value(); }

	void cancelGeneration();

public:
	std::unique_ptr<std::array<PlanetQuadTreeNode, 4>> children;

	glm::dvec3 posPlanet;
	glm::dmat3 localToPlanet;
	std::optional<size_t> patchId;

	std::shared_ptr<std::atomic_bool> generationCancellationToken_;

	AABB aabb;
};

class PlanetQuadTree
{
	enum class LODSelectResult
	{
		Undefined,
		OutOfFrustum,
		OutOfRange,
		Selected
	};

public:
	PlanetQuadTree(const class Planet& planet, class PatchGenerationProcessor& processor, PlanetFace face);
	PlanetQuadTree(PlanetQuadTree&&) = default;

	void queryVisibleNodes(const class Frustum& frustumPlanet, std::deque<VisibleNode> &visibleNodes);

	void ensurePatchesResident(const class Frustum& frustumPlanet);
	

	void processGenerationRequest() const;

	constexpr PlanetFace face() const { return face_;};
	constexpr Transform transform() const { return to_transform(face_); }
	constexpr uint32_t maxLODLevel() const { return maxLODLevel_; };

protected:
	LODSelectResult queryVisibleNodes(const Frustum& frustum, const horizon_culling::Cone<double> &cone, std::deque<VisibleNode>& visibleNodes,
		PlanetQuadTreeNode &node,
		const glm::dvec2 &topLeft, double size, uint32_t depth, bool parentCompletelyInFrustum);

	void ensurePatchesResident(const class Frustum& frustumPlanet, PlanetQuadTreeNode& node,
		const glm::dvec2 &topLeft, double size, uint32_t depth);
	
	void merge(PlanetQuadTreeNode &node);
	void destroyLeaf(PlanetQuadTreeNode &node);
	void initializeLeaf(PlanetQuadTreeNode& node, const glm::dvec2 &topLeft, double size, uint32_t lodLevel);


private:
	const class Planet& planet_;
	uint32_t maxLODLevel_;

	//const class PlanetGenerator& generator_;
	class PatchGenerationProcessor& processor_;
	PlanetFace face_;
	PlanetQuadTreeNode root_;

	std::vector<double> splitDistances_;
};
