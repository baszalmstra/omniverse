#pragma once

#include <cstdint>
#include <common/gl_resource.h>

/**
 * Represents everything that is needed to render the geometry of a mesh.
 */
struct MeshGeometry
{
	GLBufferPtr vertexBuffer;
	GLBufferPtr indexBuffer;
	GLVertexArrayPtr vertexArrayObject;
	uint32_t vertexCount;
	uint32_t indexCount;
	GLenum indexType;
	GLenum primitiveType;
};

namespace mesh
{
	/**
	 * @brief Given a mesh draw it to the screen by issueing a draw call through OpenGL.
	 * @note The user must ensure the rest of the OpenGL pipeline is properly setup to
	 *	handle proper rendering.
	 */
	void draw(const MeshGeometry& mesh);
}