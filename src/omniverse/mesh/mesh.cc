#include "mesh.h"
#include <glad/glad.h>

//-------------------------------------------------------------------------------------------------
void mesh::draw(const MeshGeometry& mesh)
{
	glBindVertexArray(*mesh.vertexArrayObject);
	if(mesh.indexCount > 0)
		glDrawElements(mesh.primitiveType, mesh.indexCount, mesh.indexType, 0);
	else
		glDrawArrays(mesh.primitiveType, 0, mesh.vertexCount);
	glBindVertexArray(0);
}
