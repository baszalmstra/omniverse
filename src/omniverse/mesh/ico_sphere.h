#pragma once

#include "mesh.h"

namespace mesh
{
	/**
	 * Generates a sphere by recursively subdividing an icosahedron.
	 * @param recursionLevel The number of times a triangle on the icosahedron is subdivided.
	 */
	MeshGeometry generate_ico_sphere(uint32_t recursionLevel);
}