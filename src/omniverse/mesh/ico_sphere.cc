#include "ico_sphere.h"
#include <cmath>
#include <vector>
#include <glm/glm.hpp>
#include <functional>

//-------------------------------------------------------------------------------------------------
// Reference: http://blog.andreaskahler.com/2009/06/creating-icosphere-mesh-in-code.html
MeshGeometry mesh::generate_ico_sphere(uint32_t recursionLevel)
{
	struct Triangle { uint32_t a,b,c; };

	std::vector<glm::vec3> positions;
	std::vector<Triangle> faces;
	std::unordered_map<uint64_t, uint32_t> middlePointIndexCache;

	positions.reserve(12);
	faces.reserve(20);
	
	const auto addVertex = [&](const glm::vec3& p) { positions.emplace_back(glm::normalize(p)); return static_cast<uint32_t>(positions.size()-1); };
	const auto getMiddlePoint = [&](uint32_t a, uint32_t b) {
		// First check if we have it already
		const bool firstIsSmaller = a < b;
		const uint64_t smallerIndex = firstIsSmaller ? a : b;
		const uint64_t greaterIndex = firstIsSmaller ? b : a;
		const uint64_t key = (smallerIndex << 32) + greaterIndex;

		const auto it = middlePointIndexCache.find(key);
		if(it != middlePointIndexCache.end())
			return it->second;

		// Calculate it
		const glm::vec3 pointA = positions.data()[a];
		const glm::vec3 pointB = positions.data()[b];
		const glm::vec3 middle = (pointA + pointB) * 0.5f;
		uint32_t index = addVertex(middle);

		// Cache it
		middlePointIndexCache.emplace(key, index);
		return index;
	};

	// create 12 vertices of a icosahedron
	const double t = (1.0 + std::sqrt(5.0)) / 2.0;

	addVertex(glm::vec3(-1.0f,  t,  0.0f));
	addVertex(glm::vec3( 1.0f,  t,  0.0f));
	addVertex(glm::vec3(-1.0f, -t,  0.0f));
	addVertex(glm::vec3( 1.0f, -t,  0.0f));

	addVertex(glm::vec3( 0.0f, -1.0f,  t));
	addVertex(glm::vec3( 0.0f,  1.0f,  t));
	addVertex(glm::vec3( 0.0f, -1.0f, -t));
	addVertex(glm::vec3( 0.0f,  1.0f, -t));

	addVertex(glm::vec3( t,  0.0f, -1.0f));
	addVertex(glm::vec3( t,  0.0f,  1.0f));
	addVertex(glm::vec3(-t,  0.0f, -1.0f));
	addVertex(glm::vec3(-t,  0.0f,  1.0f));

	// create 20 triangles of the icosahedron

  // 5 faces around point 0
	faces.emplace_back(Triangle {0, 11, 5});
	faces.emplace_back(Triangle {0, 5, 1});
	faces.emplace_back(Triangle {0, 1, 7});
	faces.emplace_back(Triangle {0, 7, 10});
	faces.emplace_back(Triangle {0, 10, 11});

  // 5 adjacent faces 
	faces.emplace_back(Triangle {1, 5, 9});
	faces.emplace_back(Triangle {5, 11, 4});
	faces.emplace_back(Triangle {11, 10, 2});
	faces.emplace_back(Triangle {10, 7, 6});
	faces.emplace_back(Triangle {7, 1, 8});

  // 5 faces around point 3
	faces.emplace_back(Triangle {3, 9, 4});
	faces.emplace_back(Triangle {3, 4, 2});
	faces.emplace_back(Triangle {3, 2, 6});
	faces.emplace_back(Triangle {3, 6, 8});
	faces.emplace_back(Triangle {3, 8, 9});

  // 5 adjacent faces 
	faces.emplace_back(Triangle {4, 9, 5});
	faces.emplace_back(Triangle {2, 4, 11});
	faces.emplace_back(Triangle {6, 2, 10});
	faces.emplace_back(Triangle {8, 6, 7});
	faces.emplace_back(Triangle {9, 8, 1});

	// Refine triangles
	for(uint32_t i = 0; i < recursionLevel; ++i)
	{
		std::vector<Triangle> faces2;
		faces2.reserve(faces.size()*4);

		for(const Triangle& triangle : faces)
		{
			// replace triangle by 4 triangles
      uint32_t a = getMiddlePoint(triangle.a, triangle.b);
      uint32_t b = getMiddlePoint(triangle.b, triangle.c);
      uint32_t c = getMiddlePoint(triangle.c, triangle.a);

			faces2.emplace_back(Triangle {triangle.a, a, c});
			faces2.emplace_back(Triangle {triangle.b, b, a});
			faces2.emplace_back(Triangle {triangle.c, c, b});
			faces2.emplace_back(Triangle {a, b, c});
		}

		faces = std::move(faces2);
		middlePointIndexCache.clear();
	}

	MeshGeometry mesh;
	mesh.primitiveType = GL_TRIANGLES;

	// Generate the vertex buffer
	mesh.vertexCount = static_cast<uint32_t>(positions.size());
	mesh.vertexBuffer = GLBufferPtr::Create();
	glBindBuffer(GL_ARRAY_BUFFER, *mesh.vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, positions.size()*sizeof(glm::vec3), positions.data(), GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Generate the index buffer
	mesh.indexCount = static_cast<uint32_t>(faces.size()*3);
	mesh.indexType = GL_UNSIGNED_INT;
	mesh.indexBuffer = GLBufferPtr::Create();
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *mesh.indexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, faces.size()*sizeof(Triangle), faces.data(), GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// Generate the vertex array object
	mesh.vertexArrayObject = GLVertexArrayPtr::Create();
	glBindVertexArray(*mesh.vertexArrayObject);
	glEnableVertexAttribArray(0);
	glVertexAttribFormat(0, 3, GL_FLOAT, GL_FALSE, 0);
	glVertexAttribBinding(0, 0);
	glBindVertexBuffer(0, *mesh.vertexBuffer, 0, sizeof(glm::vec3));
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *mesh.indexBuffer);
	glBindVertexArray(0);

	return mesh;
}
