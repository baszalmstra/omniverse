#include <glad/glad.h>
#include "planet_patch.h"

#include <glm/vec2.hpp>
#include <vector>

//-------------------------------------------------------------------------------------------------
PlanetPatch::PlanetPatch(uint32_t numVertices) :
	vertexBuffer(GLBufferPtr::Create()),
	indexBuffer(GLBufferPtr::Create())
{
	{
		// Generate vertices
		std::vector<glm::vec2> vertices;
		vertices.resize(numVertices*numVertices);
		const float step = 2.f / (numVertices-1);
		for(size_t y = 0; y < numVertices; ++y)
			for(size_t x = 0; x < numVertices; ++x)
				vertices[y*numVertices+x] = glm::vec2(x*step-1.f, y*step-1.f);

		// Store the data
		glBindBuffer(GL_ARRAY_BUFFER, *vertexBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * vertices.size(), vertices.data(), GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		
		vertexCount = numVertices*numVertices;
	}
	{
		// Generate indices
		std::vector<uint16_t> indices;
		indices.resize((numVertices-1)*(numVertices-1)*6);
		for(size_t y = 0; y < numVertices-1; ++y)
			for(size_t x = 0; x < numVertices-1; ++x)
			{
				const size_t offset = (y*(numVertices-1)+x) * 6;
				indices[offset + 0] = static_cast<uint16_t>(x + y*numVertices);
				indices[offset + 1] = static_cast<uint16_t>((x+1) + (y+1)*numVertices);
				indices[offset + 2] = static_cast<uint16_t>(x + (y+1)*numVertices);

				indices[offset + 3] = static_cast<uint16_t>((x+1) + (y+1)*numVertices);
				indices[offset + 4] = static_cast<uint16_t>(x + y*numVertices);
				indices[offset + 5] = static_cast<uint16_t>((x+1) + y*numVertices);
			}

		// Store the data
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *indexBuffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint16_t) * indices.size(), indices.data(), GL_STATIC_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		indexCount = (numVertices-1)*(numVertices-1)*6;
	}
}
