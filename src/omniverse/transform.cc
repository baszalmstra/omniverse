#include "transform.h"
#include<glm/gtc/quaternion.hpp>
#include "glm/gtc/matrix_transform.inl"

//-------------------------------------------------------------------------------------------------
void Transform::setOrientation(const glm::dquat & orientation)
{
	transformation_.reset();
	inverseTransformation_.reset();
	orientation_ = orientation;
}

//-------------------------------------------------------------------------------------------------
void Transform::setTranslation(const glm::dvec3 & position)
{
	transformation_.reset();
	inverseTransformation_.reset();
	translation_ = position;
}

//-------------------------------------------------------------------------------------------------
void Transform::setScale(const glm::dvec3 & scale)
{
	transformation_.reset();
	inverseTransformation_.reset();
	scale_ = scale;
}

//-------------------------------------------------------------------------------------------------
const glm::mat4 & Transform::transformation() const
{
	if(!transformation_.has_value())
		transformation_ = glm::translate(glm::dmat4(1.f), translation_) * glm::mat4_cast(glm::dquat(orientation_)) * glm::scale(glm::dmat4(1), scale_);
	return *transformation_;
}

//-------------------------------------------------------------------------------------------------
const glm::mat4 & Transform::inverseTransformation() const
{
	if(!inverseTransformation_.has_value())
		inverseTransformation_ = glm::scale(glm::dmat4(1), 1.0/scale_) * glm::mat4_cast(glm::inverse(orientation_)) * glm::translate(glm::dmat4(1.f), -glm::dvec3(translation_));
	return *inverseTransformation_;
}


