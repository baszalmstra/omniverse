#pragma once

#include "aabb.h"
#include <glm/vec3.hpp>

namespace horizon_culling
{
	// Implementation based on: https://cesium.com/blog/2013/04/25/horizon-culling/

	template<typename T>
	class Cone
	{
	public:
		Cone(const glm::tvec3<T> &cameraToCenter, T planetRadius);

		bool isInside(const glm::tvec3<T>& position) const;
		bool isInside(const AABB& aabb) const;

		glm::tvec3<T> origin;
		glm::tvec3<T> direction;
		T nearDistance;
		T cosAngle;
	};

	template<typename T>
	Cone<T>::Cone(const glm::tvec3<T> &cameraPlanet, T planetRadius) :
		origin(cameraPlanet), direction(glm::normalize(-cameraPlanet))
	{
		T distanceToCenter = glm::length(cameraPlanet);
		T radiusSquared = planetRadius * planetRadius;
		T distanceFromCenterToPlane = radiusSquared / distanceToCenter;
		nearDistance = distanceToCenter - distanceFromCenterToPlane;
		cosAngle = nearDistance /std::sqrt(distanceToCenter*distanceToCenter - radiusSquared);
	}

	template <typename T>
	bool Cone<T>::isInside(const glm::tvec3<T>& position) const
	{
		auto positionCamera = position - origin;
		return glm::dot(direction, positionCamera) > nearDistance &&
			glm::dot(direction, positionCamera)/glm::length(positionCamera) > cosAngle;
	}

	template <typename T>
	bool Cone<T>::isInside(const AABB& aabb) const
	{
		glm::tvec3<T> corners[8];
		aabb.getCorners(corners);

		for(int i = 0; i < 8; ++i)
		{
			if (!isInside(corners[i]))
				return false;
		}

		return true;
	}
}
