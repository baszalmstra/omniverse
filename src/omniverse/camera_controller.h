#pragma once

#include <glm/glm.hpp>
#include "planet.h"

class CameraController
{
public:
	CameraController(struct GLFWwindow *window, class Camera& camera);

	void keyCallback(int key, int scancode, int action, int mods);
	void mouseButtonCallback(int button, int action, int mods);
	void mouseScrollCallback(double xoffset, double yoffset);
	void mouseMoveCallback(double x, double y);

	void update(float dt, const Planet& planet);

	bool isMouseCaptured() const { return mouseCaptured_; }

	double movementSpeed() const { return movementSpeed_; }

protected:
	void setCaptureMouse(bool capture);
	glm::dvec2 mousePosition() const;

private:
	struct GLFWwindow *window_;
	Camera &camera_;
	double movementSpeed_ = 1000.0;

	glm::dvec3 movementVector_ = glm::dvec3(0,0,0);
	glm::dvec2 lastMousePosition_, mouseDelta_;

	bool mouseCaptured_ = false;
};
