#include "camera.h"
#include "aabb.h"
#include "glm/gtc/matrix_transform.hpp"

//-------------------------------------------------------------------------------------------------
Frustum Camera::buildFrustum(float aspectRatio) const
{
	return Frustum(transform_, glm::perspective(fieldOfView_,aspectRatio, nearZ_, farZ_));
}

//-------------------------------------------------------------------------------------------------
Containment Frustum::classify(const AABB & aabb) const
{
	glm::dvec3 corners[8];
	uint32_t totalCornersInside = 0;

	// get the corners of the box into the vCorner array
	aabb.getCorners(corners);

	// test all 8 corners against the 6 sides 
	// if all points are behind 1 specific plane, we are out
	// if we are in with all points, then we are fully in
	for(uint32_t p = 0; p < 6; ++p)
	{
		int inCount = 8;
		int pointsInside = 1;

		for(int i = 0; i < 8; ++i) {

			// test this point against the planes
			if(planes_[p].classify(corners[i]) == PlaneHalfspace::Negative) 
			{
				pointsInside = 0;
				--inCount;
			}
		}

		// were all the points outside of plane p?
		if(inCount == 0)
			return Containment::Outside;

		// check if they were all on the right side of the plane
		totalCornersInside += pointsInside;
	}

	// so if iTotalIn is 6, then all are inside the view
	if(totalCornersInside == 6)
		return Containment::Inside;

	// we must be partly in then otherwise
	return Containment::Intersects;
}

//-------------------------------------------------------------------------------------------------
Containment Frustum::classify(const glm::vec3& pos, float radius) const
{
	// calculate our distances to each of the planes
	for(int i = 0; i < 6; ++i) {

		// find the distance to this plane
		float distance = planes_[i].distance(pos);

		// if this distance is < -sphere.radius, we are outside
		if(distance < -radius)
			return Containment::Outside;

		// else if the distance is between +- radius, then we intersect
		if(std::fabsf(distance) < radius)
			return Containment::Intersects;
	}

	// otherwise we are fully in view
	return Containment::Inside;
}

//-------------------------------------------------------------------------------------------------
bool Frustum::intersects(const AABB & aabb) const
{
	return classify(aabb) != Containment::Outside;
}

//-------------------------------------------------------------------------------------------------
bool Frustum::intersects(const glm::vec3& pos, float radius) const
{
	return classify(pos, radius) != Containment::Outside;
}

//-------------------------------------------------------------------------------------------------
void Frustum::computeFrustumPlanesFromViewProjection()
{
	// Left clipping plane
	planes_[0].a = viewProjection_[0][3] + viewProjection_[0][0];
	planes_[0].b = viewProjection_[1][3] + viewProjection_[1][0];
	planes_[0].c = viewProjection_[2][3] + viewProjection_[2][0];
	planes_[0].d = viewProjection_[3][3] + viewProjection_[3][0];

	// Right clipping plane
	planes_[1].a = viewProjection_[0][3] - viewProjection_[0][0];
	planes_[1].b = viewProjection_[1][3] - viewProjection_[1][0];
	planes_[1].c = viewProjection_[2][3] - viewProjection_[2][0];
	planes_[1].d = viewProjection_[3][3] - viewProjection_[3][0];

	// Top clipping plane
	planes_[2].a = viewProjection_[0][3] - viewProjection_[0][1];
	planes_[2].b = viewProjection_[1][3] - viewProjection_[1][1];
	planes_[2].c = viewProjection_[2][3] - viewProjection_[2][1];
	planes_[2].d = viewProjection_[3][3] - viewProjection_[3][1];

	// Bottom clipping plane
	planes_[3].a = viewProjection_[0][3] + viewProjection_[0][1];
	planes_[3].b = viewProjection_[1][3] + viewProjection_[1][1];
	planes_[3].c = viewProjection_[2][3] + viewProjection_[2][1];
	planes_[3].d = viewProjection_[3][3] + viewProjection_[3][1];

	// Near clipping plane
	planes_[4].a = viewProjection_[0][3] + viewProjection_[0][2];
	planes_[4].b = viewProjection_[1][3] + viewProjection_[1][2];
	planes_[4].c = viewProjection_[2][3] + viewProjection_[2][2];
	planes_[4].d = viewProjection_[3][3] + viewProjection_[3][2];

	// Far clipping plane
	planes_[5].a = viewProjection_[0][3] - viewProjection_[0][2];
	planes_[5].b = viewProjection_[1][3] - viewProjection_[1][2];
	planes_[5].c = viewProjection_[2][3] - viewProjection_[2][2];
	planes_[5].d = viewProjection_[3][3] - viewProjection_[3][2];
}
