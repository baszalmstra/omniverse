#include "planet_patch_buffer.h"
#include "planet_quad_tree.h"

//-------------------------------------------------------------------------------------------------
GLsizei sizeForFormat(GLenum internalFormat)
{
	switch(internalFormat)
	{
	case GL_R32F:
		return 4;
	case GL_RGB32F:
		return 12;
	default:
		assert(false);
		return 0;
	}
}

//-------------------------------------------------------------------------------------------------
// PatchGeometryBuffer
//-------------------------------------------------------------------------------------------------
PatchGeometryBuffer::PatchGeometryBuffer(size_t maxPatchCount, size_t bytesPerPatch) : 
	maxPatchCount_(maxPatchCount),
	bytesPerPatch_(bytesPerPatch)
{
	
}

//-------------------------------------------------------------------------------------------------
bool PatchGeometryBuffer::init()
{
	const size_t totalSize = bytesPerPatch_*maxPatchCount_;

	buffer_ = GLBufferPtr::Create();
	glBindBuffer(GL_ARRAY_BUFFER, *buffer_);
	//glBufferStorage(GL_ARRAY_BUFFER, totalSize, nullptr, GL_MAP_WRITE_BIT);
	glBufferData(GL_ARRAY_BUFFER, totalSize, nullptr, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	return buffer_ && glGetError() == GL_NO_ERROR;;
}

//-------------------------------------------------------------------------------------------------
void *PatchGeometryBuffer::map(size_t id)
{
	const size_t patchSizeInBytes = (PlanetQuadTreeNode::VerticesPerPatch)*(PlanetQuadTreeNode::VerticesPerPatch) * sizeof(PlanetVertex);
	size_t offset = id*patchSizeInBytes;

	glBindBuffer(GL_ARRAY_BUFFER, *buffer_);
	void *ptr = glMapBufferRange(GL_ARRAY_BUFFER, offset, patchSizeInBytes, GL_MAP_WRITE_BIT|GL_MAP_INVALIDATE_RANGE_BIT|GL_MAP_FLUSH_EXPLICIT_BIT);
	assert(ptr != nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	return ptr;
}

//-------------------------------------------------------------------------------------------------
void PatchGeometryBuffer::unmap(size_t /*id*/)
{
	const size_t patchSizeInBytes = (PlanetQuadTreeNode::VerticesPerPatch)*(PlanetQuadTreeNode::VerticesPerPatch) * sizeof(PlanetVertex);

	glBindBuffer(GL_ARRAY_BUFFER, *buffer_);
	glFlushMappedBufferRange(GL_ARRAY_BUFFER, 0, patchSizeInBytes);
	glUnmapBuffer(GL_ARRAY_BUFFER);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

//-------------------------------------------------------------------------------------------------
const GLBufferPtr& PatchGeometryBuffer::buffer() const
{
	return buffer_;
}

//-------------------------------------------------------------------------------------------------
// PatchTexture
//-------------------------------------------------------------------------------------------------
PatchTextureAtlas::PatchTextureAtlas(size_t maxPatchCount, uint32_t levels, size_t texelsPerPatch, GLenum format) :
	format_(format),
	maxPatchCount_(maxPatchCount), texelsPerPatch_(texelsPerPatch), levels_(levels)
{
}

//-------------------------------------------------------------------------------------------------
bool PatchTextureAtlas::init()
{
	patchStride_ = static_cast<size_t>(std::sqrt(static_cast<double>(maxPatchCount_)));
	GLint maxTextureSize, requestedSize = static_cast<GLint>(patchStride_ * texelsPerPatch_);
	glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maxTextureSize);
	if(requestedSize > maxTextureSize)
		return false;	
	
	texture_ = GLTexturePtr::Create();
	glBindTexture(GL_TEXTURE_2D, *texture_);
	glTexStorage2D(GL_TEXTURE_2D, levels_, format_, requestedSize, requestedSize);
	glBindTexture(GL_TEXTURE_2D, 0);

	for(auto& transferBuffer : transferBuffers_)
	{
		transferBuffer = GLBufferPtr::Create();
		if(!transferBuffer)
			return false;
	}

	return texture_ && glGetError() == GL_NO_ERROR;
}

//-------------------------------------------------------------------------------------------------
void PatchTextureAtlas::write(size_t id, uint32_t level, const av::array_view<uint8_t>& data, GLenum format, GLenum type)
{
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, *transferBuffers_[nextTransferBuffer_]);
	nextTransferBuffer_ = (nextTransferBuffer_ + 1) % transferBuffers_.size();
	glBufferData(GL_PIXEL_UNPACK_BUFFER, data.size(), data.data(), GL_STREAM_DRAW);

  const size_t levelSize = texelsPerPatch_ >> level;

	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
  glPixelStorei(GL_UNPACK_ROW_LENGTH, static_cast<GLint>(levelSize));
  glPixelStorei(GL_UNPACK_SKIP_PIXELS, 0);
  glPixelStorei(GL_UNPACK_SKIP_ROWS, 0);
  
	const size_t patchX = id % patchStride_;
	const size_t patchY = id / patchStride_;
	glBindTexture(GL_TEXTURE_2D, *texture_);
	glTexSubImage2D(GL_TEXTURE_2D, level, 
		static_cast<GLint>(patchX*levelSize),
		static_cast<GLint>(patchY*levelSize),
		static_cast<GLsizei>(levelSize),
		static_cast<GLsizei>(levelSize),
		format, type, static_cast<void*>(0));
		
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
}

//-------------------------------------------------------------------------------------------------
// PlanetPatchBuffer
//-------------------------------------------------------------------------------------------------
PlanetPatchBuffer::PlanetPatchBuffer(size_t maxPatchCount) :
	geometryBuffer_(maxPatchCount, (PlanetQuadTreeNode::VerticesPerPatch)*(PlanetQuadTreeNode::VerticesPerPatch)),
	normalTextureAtlas_(maxPatchCount, 1, PlanetQuadTreeNode::NormalsPerPatch),
	heightTextureAtlas_(maxPatchCount, 1, PlanetQuadTreeNode::VerticesPerPatch)
{
	availableSections_.resize(maxPatchCount);
	for(size_t i = 0; i < maxPatchCount-1; ++i)
		availableSections_[i] = i+1;
	availableSections_[availableSections_.size()-1] = 0xffffffff;
	availableSectionTail_ = maxPatchCount-1;
	availableSectionHead_ = 0;
	totalAcquired_ = 0;
}

//-------------------------------------------------------------------------------------------------
bool PlanetPatchBuffer::init()
{
	patchStride_ = static_cast<size_t>(std::sqrt(static_cast<double>(availableSections_.size())));

	if(!geometryBuffer_.init())
		return false;

	if(!normalTextureAtlas_.init())
		return false;

	glBindTexture(GL_TEXTURE_2D, normalTextureAtlas_.texture());
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	
	if(!heightTextureAtlas_.init())
		return false;

	glBindTexture(GL_TEXTURE_2D, heightTextureAtlas_.texture());
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);

	glBindTexture(GL_TEXTURE_2D, 0);

	return true;
}

//-------------------------------------------------------------------------------------------------
size_t PlanetPatchBuffer::acquire()
{
	assert(availableSectionHead_ != 0xffffffff);
	
	size_t nextSectionHead_ = availableSections_[availableSectionHead_];
	size_t result = availableSectionHead_;
	availableSectionHead_ = nextSectionHead_;
	totalAcquired_++;
	return result;
}

//-------------------------------------------------------------------------------------------------
void PlanetPatchBuffer::release(size_t id)
{
	// Push back case: 
	/*availableSections_[availableSectionTail_] = id;
	availableSectionTail_ = id;*/

	// Push front case:
	availableSections_[id] = availableSectionHead_;
	availableSectionHead_ = id;	
	totalAcquired_--;
}

//-------------------------------------------------------------------------------------------------
size_t PlanetPatchBuffer::baseVertex(size_t id) const
{
	return (PlanetQuadTreeNode::VerticesPerPatch)*(PlanetQuadTreeNode::VerticesPerPatch)*id;
}