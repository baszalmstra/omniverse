#pragma once

#include "planet_quad_tree.h"
#include "planet.h"
#include "planet_patch_buffer.h"
#include <vector>
#include <thread>
#include <mutex>
#include <atomic>
#include <condition_variable>

/**
 * Defines the coordinates of a single patch on a face of the planet. Patches are defined by the 
 * uv coordinates relative to a face of the planet.
 */
struct PlanetPatch
{
	PlanetFace face;
	glm::dvec2 topLeftUv;
	double sizeUv;
};

/**
 * A class that is used by a planet to generate geometric vertex data for patches.
 */
class PlanetGenerator
{
public:
	explicit PlanetGenerator(const class Planet& planet);
	~PlanetGenerator();

	/**
	 * Called to generate the geometry for a single patch. This function is reentrant.
	 * @param patch The patch to generate geometry for
	 * @param[out] vertices The positions for the vertices
	 * @param[out] normals The normals for the vertices
	 * @param cancellationToken A boolean that indicates whether or not the operation should be 
	 *	cancelled. True if the operation should be cancelled; false otherwise. This value can 
	 *	be modified by another thread.
	 */
	void generatePatch(const PlanetPatch &patch, 
		std::vector<glm::dvec3>& vertices, std::vector<glm::vec3>& normals,
		std::atomic_bool &cancellationToken) const;

private:
	const class Planet& planet_;
	std::unique_ptr<class FastNoise> noise_;
};


/**
 * Generation struct for the producer consumer
 */
struct GenerationInput
{
	PlanetQuadTreeNode* node;

	PlanetPatch patch;

	uint32_t lodLevel;
	
	std::vector<glm::vec3> planetNormals;
	std::vector<glm::dvec3> planetPositions;

	std::atomic_bool cancellationToken = false;
};

using GenerationInputPtr = typename std::shared_ptr<GenerationInput>;

class PatchGenerationProcessor
{
public:
		explicit PatchGenerationProcessor(const Planet& planet, uint32_t num_of_workers);
		~PatchGenerationProcessor();

		void queue(const GenerationInputPtr& ptr);

		std::deque<GenerationInputPtr> retrieveResults();

private:
		/// Retrieve a GenerationInput to calculate from the processor
		GenerationInputPtr pop();

		/// Retrieve a GenerationInput to calculate from the processor
		void push(GenerationInputPtr& pointer);

private:
		class ThreadGenerator {

		public:
				ThreadGenerator(const PlanetGenerator& generator, PatchGenerationProcessor* processor);
				~ThreadGenerator();
				
		private:
				void calculate();

		private:
				const PlanetGenerator& generator_;
				PatchGenerationProcessor* processor_;
				std::atomic_bool stop_, stopped_;
				std::thread thread_;
				std::condition_variable stoppedCondition_;
				std::mutex stoppedMutex_;
		};

		/// Process queues going in and out
		std::deque<GenerationInputPtr> in_;
		std::deque<GenerationInputPtr> out_;

		std::vector<std::unique_ptr<ThreadGenerator>> workers_;
		std::mutex in_mutex_, out_mutex_;
		std::condition_variable in_notify_;

		PlanetGenerator generator_;
};


