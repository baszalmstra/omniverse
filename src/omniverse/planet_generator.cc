#include "planet_generator.h"

#include "FastNoise.h"

//-------------------------------------------------------------------------------------------------
namespace
{
	glm::dvec3 morph(const glm::dvec3& pos)
	{
		glm::dvec3 posSq = pos * pos;
		glm::dvec3 a = glm::dvec3(posSq[1], posSq[2], posSq[0]) * 0.5;
		glm::dvec3 b = glm::dvec3(posSq[2], posSq[0], posSq[1]) * 0.5;
		glm::dvec3 c = glm::dvec3(posSq[1], posSq[2], posSq[0]) * glm::dvec3(posSq[2], posSq[0], posSq[1]) / 3.0;
		return pos * sqrt(glm::dvec3(1.0) - a - b + c);
	}

	/**
	 * @brief computeHeight
	 * @param x Position between 0 and 1
	 * @param y Position between 0 and 1
	 * @return Height
	 */
	double computeHeight(const FastNoise& noise, const glm::vec3& p)
	{
		double height = 800 * noise.GetNoise(p.x / 100, p.y / 100, p.z / 100);
//		std::cout << height << std::endl;
		return height;
//		return ((int(x * 10) + int(y * 10)) % 2) * 10000;
//		return 500 * sin(250 * sqrt(x * x + y * y)) + 50;// - 1000;
	}

	glm::dvec3 computeVertex(const Planet& planet, const Transform& transform, const FastNoise& noise, double x, double y)
	{
		glm::dvec3 pPlanetCube = transform * glm::dvec3(x, y, 1);
		glm::dvec3 p = morph(pPlanetCube);
		glm::dvec3 pSphere = p * planet.radius();
		double height = computeHeight(noise, pSphere);
		return pSphere + p * height;
	}

	glm::vec3 computeNormal(const Planet& planet, const Transform& transform, const FastNoise& noise, double x, double y, double eps)
	{
		glm::vec3 px1 = computeVertex(planet, transform, noise, x - eps, y);
		glm::vec3 px2 = computeVertex(planet, transform, noise, x + eps, y);
		glm::vec3 py1 = computeVertex(planet, transform, noise, x, y - eps);
		glm::vec3 py2 = computeVertex(planet, transform, noise, x, y + eps);

		glm::vec3 n = glm::cross(px2 - px1, py2 - py1);

		return glm::normalize(n);
	}

}

//-------------------------------------------------------------------------------------------------
PlanetGenerator::PlanetGenerator(const class Planet& planet) : planet_(planet)
{
	noise_ = std::make_unique<FastNoise>();
	noise_->SetNoiseType(FastNoise::SimplexFractal);
}

//-------------------------------------------------------------------------------------------------
PlanetGenerator::~PlanetGenerator()
{

}

//-------------------------------------------------------------------------------------------------
void PlanetGenerator::generatePatch(const PlanetPatch &patch, 
	std::vector<glm::dvec3>& vertices, std::vector<glm::vec3>& normals,
	std::atomic_bool &cancellationToken) const
{
	const Transform poseFace = to_transform(patch.face);

	const uint32_t VerticesPerPatch = PlanetQuadTreeNode::VerticesPerPatch;
	const uint32_t NormalsPerPatch = PlanetQuadTreeNode::NormalsPerPatch;

	assert(vertices.size() == VerticesPerPatch*VerticesPerPatch);
	assert(normals.size() == NormalsPerPatch*NormalsPerPatch);

//	const uint32_t VertexCount = VerticesPerPatch*VerticesPerPatch;
	const double VertexStep = patch.sizeUv / (VerticesPerPatch - 1);
	const double NormalStep = patch.sizeUv / (NormalsPerPatch - 1);

	std::size_t k = 0;

	// Generate vertices
	for(std::size_t iy = 0; iy < VerticesPerPatch; ++iy)
	{
		if(cancellationToken)
			return;

		double y = std::max(0.0, std::min(1.0, patch.topLeftUv.y + iy * VertexStep)) * 2.0 - 1.0;
		for(std::size_t ix = 0; ix < VerticesPerPatch; ++ix)
		{
			double x = std::max(0.0, std::min(1.0, patch.topLeftUv.x + ix * VertexStep)) * 2.0 - 1.0;
			vertices[k] = computeVertex(planet_, poseFace, *noise_, x, y);
//			glm::dvec3 pPlanetCube = transform() * glm::dvec3(x, y, 1);
//            double height = 500 * sin(250 * sqrt(x * x + y * y)) - 1000;
//            planetPositions[k++] = morph(pPlanetCube) * (planet_.radius() + height);
			++k;
		}
	}

	if(cancellationToken)
		return;

	// Generate normals
	k = 0;
	for(std::size_t iy = 0; iy < NormalsPerPatch; ++iy)
	{
		if(cancellationToken)
			return;

		double y = std::max(0.0, std::min(1.0, patch.topLeftUv.y + iy * NormalStep)) * 2.0 - 1.0;
		for(std::size_t ix = 0; ix < NormalsPerPatch; ++ix)
		{
			double x = std::max(0.0, std::min(1.0, patch.topLeftUv.x + ix * NormalStep)) * 2.0 - 1.0;
			normals[k] = computeNormal(planet_, poseFace, *noise_, x, y, 0.001);
			++k;
		}
	}
}

//-------------------------------------------------------------------------------------------------
PatchGenerationProcessor::PatchGenerationProcessor( const Planet& planet, uint32_t num_of_workers)
: generator_(planet)
{
	workers_.reserve(num_of_workers);
	for(uint8_t workers = 0; workers < num_of_workers; workers++)
		workers_.emplace_back(std::make_unique<ThreadGenerator>(generator_, this));
}

//-------------------------------------------------------------------------------------------------
void PatchGenerationProcessor::queue(const GenerationInputPtr &ptr)
{
	std::lock_guard input_lock(in_mutex_);
	in_.push_back(ptr);
	in_notify_.notify_one();
}

//-------------------------------------------------------------------------------------------------
std::deque<GenerationInputPtr> PatchGenerationProcessor::retrieveResults()
{
	std::lock_guard output_lock(out_mutex_);
	auto result = out_;
	out_.clear();
	return result;
}

//-------------------------------------------------------------------------------------------------
GenerationInputPtr PatchGenerationProcessor::pop() 
{
	std::unique_lock input_lock(in_mutex_);
	if(in_notify_.wait_for(input_lock, std::chrono::milliseconds(100), [&]{ return !in_.empty(); }))
	{
		auto it = std::min_element(in_.begin(), in_.end(), [&](const auto& a, const auto& b) { return a->lodLevel < b->lodLevel;});
		const GenerationInputPtr pointer = std::move(*it);
		in_.erase(it);		
		return pointer;
	}
	else
		return nullptr;
}

//-------------------------------------------------------------------------------------------------
void PatchGenerationProcessor::push(GenerationInputPtr& pointer)
{
	std::lock_guard input_lock(out_mutex_);
	out_.push_back(pointer);
}

//-------------------------------------------------------------------------------------------------
PatchGenerationProcessor::~PatchGenerationProcessor()
{
	in_notify_.notify_all();
	workers_.clear();
}

//-------------------------------------------------------------------------------------------------
void PatchGenerationProcessor::ThreadGenerator::calculate()
{
	while(!stop_)
	{
		GenerationInputPtr pointer = processor_->pop();
		if(pointer == nullptr)
			continue;

		if (pointer->cancellationToken)
			continue;

		generator_.generatePatch(pointer->patch, pointer->planetPositions, pointer->planetNormals, pointer->cancellationToken);

		processor_->push(pointer);
	}

	std::unique_lock<std::mutex> lock(stoppedMutex_);
	stopped_ = true;
	std::notify_all_at_thread_exit(stoppedCondition_, std::move(lock));
}

//-------------------------------------------------------------------------------------------------
PatchGenerationProcessor::ThreadGenerator::ThreadGenerator(const PlanetGenerator &generator, PatchGenerationProcessor* processor)
: generator_(generator), processor_(processor), stop_(false), stopped_(false)
{
	thread_ = std::thread([this]{
			calculate();
	});
}

//-------------------------------------------------------------------------------------------------
PatchGenerationProcessor::ThreadGenerator::~ThreadGenerator()
{
	stop_ = true;

	std::unique_lock<std::mutex> lock(stoppedMutex_);
	stoppedCondition_.wait(lock, [&]() -> bool { return stopped_; });

	thread_.join();
}
