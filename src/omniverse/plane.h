#pragma once

enum class PlaneHalfspace
{
	Negative = -1,
	OnPlane = 0,
	Positive = 1,
};

template<typename T>
struct TPlane
{
	TPlane() : a(0), b(0), c(0), d(0) {};
	TPlane(glm::tvec3<T> normal, T distance) : a(normal.x), b(normal.y), c(normal.z), d(distance) {};
	TPlane(T a, T b, T c, T d) : a(a), b(b), c(c), d(d) {};

	TPlane normalized() const;

	T distance(const glm::tvec3<T>& pos) const;
	template<typename U> PlaneHalfspace classify(const glm::tvec3<U>& pos) const;

	T a,b,c,d;
};

typedef TPlane<float> Plane;
typedef TPlane<double> DPlane;

template<typename T>
inline TPlane<T> TPlane<T>::normalized() const
{
	T mag = static_cast<T>(1.0) / std::sqrt(a * a + b * b + c * c);
	return Plane(a*mag, b*mag, c*mag, d*mag);
}

template<typename T>
inline T TPlane<T>::distance(const glm::tvec3<T>& pos) const
{
	return a*pos.x + b*pos.y + c*pos.z + d;
}

template<typename T>
template<typename U>
inline PlaneHalfspace TPlane<T>::classify(const glm::tvec3<U>& p) const
{
	U det = a*p.x + b*p.y + c*p.z + d;
	return det < 0 ? PlaneHalfspace::Negative : 
				 det > 0 ? PlaneHalfspace::Positive :
				 PlaneHalfspace::OnPlane;
}