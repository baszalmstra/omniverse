#pragma once

#include <array>
#include "transform.h"

enum class PlanetFace : uint8_t
{
	Right = 0,
	Left,
	Top,
	Bottom,
	Front,
	Back
};

/**
 * Given a PlanetFace returns a Transform that defines the orientation of the face.
 */
constexpr Transform to_transform(PlanetFace face)
{
	constexpr std::array<Transform, 6> faceToTransform = {
					Transform(glm::dvec3(0,0,0), glm::dquat(0.707106781187, -0.707106781187, 0.0, 0.0)),
					Transform(glm::dvec3(0,0,0), glm::dquat(0.707106781187, 0.707106781187, 0.0, 0.0)),
					Transform(glm::dvec3(0,0,0), glm::dquat(0.707106781187, 0.0, 0.707106781187, 0.0)),
					Transform(glm::dvec3(0,0,0), glm::dquat(0.707106781187, 0.0, -0.707106781187, 0.0)),
					Transform(glm::dvec3(0,0,0), glm::dquat(1.0, 0.0, 0.0, 0.0)),
					Transform(glm::dvec3(0,0,0), glm::dquat(0.0, 0.0, 1.0, 0.0))
	};

	return faceToTransform[static_cast<std::underlying_type<PlanetFace>::type>(face)];
}

