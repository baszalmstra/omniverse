#pragma once

#include "parameters.h"
#include <common/gl_resource.h>

namespace atmosphere::shader
{
  /**
   * Stores the locations of all uniforms used by the atmosphere.glsl shader.
   */
  struct Uniforms
  {
    int sunDir;               // The direction to the sun as seen from the planet
    int cameraPos;            // Position of the camera relative to the planet
    int innerRadius;          // The distance from the center of the planet to sea level
    int outerRadius;          // The distance from the center of the planet to the outer edge of the atmosphere
    int scaleDepth;           // The relative distance from the inner to the outer radius at which the average atmosphere density is found.
    int scale;                // 1.0/(outerRadius-innerRadius)
    int scaleOverScaleDepth;  // scale/scaleDepth
    int KrESun;               // Kr*ESun          
    int KmESun;               // Km*ESun
    int Kr4Pi;                // Kr*4.0*π
    int Km4Pi;                // Km*4.0*π
    int InvWavelength;        // 1.0/pow(waveLengths,4.0)
    int PhaseConstant;        // The mie phase function constant
  };

  /**
   * Get the locations of all shader uniforms for atmospheric scattering.
   * @param program The OpenGL shader program to query locations from
   */
  Uniforms find_uniforms(const GLProgramPtr& program);

  /**
   * Set the values for the given uniforms to the values for the specified atmosphere.
   * @param uniforms The location of the uniforms in the shader
   * @param parameters The atmospheric properties
   * @param radius The radius of the planet
   * @param cameraPos The position of the camera relative to the planet
   * @param sunDir The direction of the sun 
   */
  void set_uniforms(const Uniforms& uniforms, const Parameters& parameters,
    float radius,
    const glm::vec3& cameraPos,
    const glm::vec3& sunDir);
}
