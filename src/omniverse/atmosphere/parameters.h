#pragma once

#include <glm/glm.hpp>

namespace atmosphere
{
  struct Parameters
  {
    float height;                     // The height of the atmosphere
    float scaleDepth      = 0.25f;    // Where to find the average atmosphere density    
    float Kr              = 0.0015f;  // Rayleigh scattering factor
    float Km              = 0.0010f;  // Mie scattering factor
    float ESun            = 20.0f;    // Sun intensity
    float phaseConstant   = -0.990f;  // The phase constant
    glm::vec3 waveLength  = glm::vec3(0.65f, 0.57f, 0.475f);  // The wavelengths of light
  };
}