#include "shader.h"

namespace atmosphere::shader
{
  //-----------------------------------------------------------------------------------------------
  Uniforms find_uniforms(const GLProgramPtr& program)
  {
    Uniforms uniforms {};
    uniforms.sunDir = program.findUniform("SunDir").value_or(-1);
    uniforms.cameraPos = program.findUniform("CameraPos").value_or(-1);
    uniforms.innerRadius = program.findUniform("AtmosphereInnerRadius").value_or(-1);
    uniforms.outerRadius = program.findUniform("AtmosphereOuterRadius").value_or(-1);
    uniforms.scaleDepth = program.findUniform("AtmosphereScaleDepth").value_or(-1);
    uniforms.scale = program.findUniform("AtmosphereScale").value_or(-1);
    uniforms.scaleOverScaleDepth = program.findUniform("AtmosphereScaleOverScaleDepth").value_or(-1);
    uniforms.KrESun = program.findUniform("KrESun").value_or(-1);
    uniforms.KmESun = program.findUniform("KmESun").value_or(-1);
    uniforms.Kr4Pi = program.findUniform("Kr4PI").value_or(-1);
    uniforms.Km4Pi = program.findUniform("Km4PI").value_or(-1);
    uniforms.InvWavelength = program.findUniform("InvWavelength").value_or(-1);
    uniforms.PhaseConstant = program.findUniform("AtmospherePhaseConstant").value_or(-1);
    return uniforms;
  }

  //-----------------------------------------------------------------------------------------------
  void set_uniforms(const Uniforms& uniforms, const Parameters& parameters,
    float radius,
    const glm::vec3& cameraPos, 
    const glm::vec3& sunDir)
  {
    const float innerRadius = radius;
    const float outerRadius = innerRadius + parameters.height;
    const float scale = 1.0f / (outerRadius - innerRadius);

    glUniform3fv(uniforms.sunDir, 1, &sunDir.x);
    glUniform3fv(uniforms.cameraPos, 1, &cameraPos.x);
    glUniform1f(uniforms.innerRadius, innerRadius);
    glUniform1f(uniforms.outerRadius, outerRadius);
    glUniform1f(uniforms.scaleDepth, parameters.scaleDepth);
    glUniform1f(uniforms.scale, scale);
    glUniform1f(uniforms.scaleOverScaleDepth, scale/parameters.scaleDepth);
    glUniform1f(uniforms.Kr4Pi, parameters.Kr*4.0f*3.141592654f);
    glUniform1f(uniforms.Km4Pi, parameters.Km*4.0f*3.141592654f);
    glUniform1f(uniforms.KrESun, parameters.Kr*parameters.ESun);
    glUniform1f(uniforms.KmESun, parameters.Km*parameters.ESun);
    glUniform3f(uniforms.InvWavelength,
      1.0f / std::pow(parameters.waveLength.x, 4.0f),
      1.0f / std::pow(parameters.waveLength.y, 4.0f),
      1.0f / std::pow(parameters.waveLength.z, 4.0f));
    glUniform1f(uniforms.PhaseConstant, parameters.phaseConstant);
  }
}
