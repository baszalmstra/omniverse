#pragma once

#include "transform.h"
#include <glm/glm.hpp>
#include <array>
#include "plane.h"

enum class Containment
{
	Outside,
	Inside,
	Intersects
};

class Frustum
{
public:
	Frustum(const Transform& transform, const glm::mat4& projection) :
		transform_(transform), projection_(projection)
	{
		viewProjection_ = projection_ * view();
		computeFrustumPlanesFromViewProjection();
	}

	const Transform& transform() const { return transform_;}

	const glm::mat4 &projection() const { return projection_; };
	const glm::mat4 &view() const { return transform_.inverseTransformation(); }
	const glm::mat4 &viewProjection() const { return viewProjection_; }

	Containment classify(const struct AABB& aab) const;
	Containment classify(const glm::vec3 &pos, float radius) const;

	bool intersects(const struct AABB& aabb) const;
	bool intersects(const glm::vec3 &pos, float radius) const;

private:
	void computeFrustumPlanesFromViewProjection();

private:
	Transform transform_;
	glm::mat4 projection_, viewProjection_;
	std::array<Plane, 6> planes_;
};

class Camera
{
public:
	const Transform &transform() const { return transform_; }
	Transform &transform() { return transform_; }

	const float & fieldOfView() const { return fieldOfView_;}
	void setFieldOfView(float fov) { fieldOfView_ = fov; }

	const float & nearZ() const { return nearZ_; }
	void setNearZ(float nearZ) { nearZ_ = nearZ; }

	const float & farZ() const { return farZ_; }
	void setFarZ(float farZ) { farZ_ = farZ; }

	Frustum buildFrustum(float aspectRatio) const;

private:
	Transform transform_;

	float fieldOfView_ = 1.f;
	float nearZ_ = 0.1f, farZ_ = 100000.f;
};
