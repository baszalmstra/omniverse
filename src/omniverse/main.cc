#include <iostream>
#include <vector>
#include <algorithm>

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/vec3.hpp>
#include "glm/gtx/norm.inl"
#include "planet.h"
#include <common/shaders.h>

#include <glm/gtc/matrix_transform.hpp>
#include <fstream>
#include "glm/gtx/rotate_vector.inl"
#include "camera.h"
#include "camera_controller.h"
#include "planet_patch_buffer.h"

#ifdef LIVEPP_BINARY_DIR
#include <LPP_API.h>
#endif

#if __linux__
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <regex>
#endif

CameraController *gCameraController = nullptr;
ImGuiIO *imguiIo = nullptr;

//-------------------------------------------------------------------------------------------------
// Error function
//-------------------------------------------------------------------------------------------------
static void error_callback(int, const char* description)
{
	std::cerr << description << std::endl;
}

//-------------------------------------------------------------------------------------------------
// Input handling
//-------------------------------------------------------------------------------------------------
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	ImGui_ImplGlfw_KeyCallback(window, key, scancode, action, mods);
	if(imguiIo->WantCaptureKeyboard)
		return;

	if(gCameraController)
		gCameraController->keyCallback(key, scancode, action, mods);
}

void mouse_pos_callback(GLFWwindow* /*window*/, double x, double y)
{
	if(gCameraController)
		gCameraController->mouseMoveCallback(x,y);
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
	ImGui_ImplGlfw_MouseButtonCallback(window, button, action, mods);
	if(imguiIo->WantCaptureMouse)
		return;

	if(gCameraController)
		gCameraController->mouseButtonCallback(button, action, mods);
}

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	ImGui_ImplGlfw_ScrollCallback(window, xoffset, yoffset);
	if(imguiIo->WantCaptureMouse)
		return;

	if(gCameraController)
		gCameraController->mouseScrollCallback(xoffset, yoffset);
}

float calulate_dpi()
{
#if __linux__
	struct passwd *pw = getpwuid(getuid());
	const char *homedir = pw->pw_dir;
	std::ifstream xresources(std::string(homedir) + "/.Xresources");

	if(xresources.is_open())
	{
		for(std::string line; std::getline(xresources, line); )
		{
			std::regex regex("Xft.dpi: ([0-9]+)");
			std::smatch groups;
			if(std::regex_match(line, groups, regex))
			{
				std::cout << groups[1].str();
				return std::stof(groups[1].str());
			}
		}
	}
#endif


	auto monitor = glfwGetPrimaryMonitor();
	int widthMM, heightMM;
	glfwGetMonitorPhysicalSize(monitor, &widthMM, &heightMM);
	const GLFWvidmode* mode = glfwGetVideoMode(monitor);

	// Screen size / physical width / cm_to_inch
	const float dpi = mode->width / (widthMM / 25.4f);

	return dpi;
}

//-------------------------------------------------------------------------------------------------
// Main function
//-------------------------------------------------------------------------------------------------
int main(void)
{
#ifdef LIVEPP_BINARY_DIR
	// load the Live++ DLL, check for version mismatch, and register process group
	HMODULE livePP = lpp::lppLoadAndRegister(LIVEPP_BINARY_DIR, "Omniverse");

  // enable Live++
  lpp::lppEnableAllCallingModulesSync(livePP);

  // enable Live++'s exception handler/error recovery
  lpp::lppInstallExceptionHandler(livePP);
#endif

	glfwSetErrorCallback(error_callback);

	if (!glfwInit())
		return 0;

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_STENCIL_BITS, 8);
	//glfwWindowHint(GLFW_SAMPLES, 4);
#if __APPLE__
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

	GLFWwindow* window = glfwCreateWindow(1280, 800, "Omniverse", NULL, NULL);
	if (!window)
		return 0;

	glfwMakeContextCurrent(window);
	gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
	glfwSwapInterval(0);

	// Setup Dear ImGui binding
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO();
	imguiIo = &io;

	// Size in pixels is pt (font-size) * dpi / 72.f (default dpi for ttf fonts)
	ImFontConfig config;
	config.SizePixels = 8 * calulate_dpi() / 72.f;
	imguiIo->Fonts->AddFontDefault(&config);
	//imguiIo->FontDefault->FontSize = 1.0;
	//io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;  // Enable Keyboard Controls
	//io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;   // Enable Gamepad Controls

	ImGui_ImplGlfw_InitForOpenGL(window, true);
	ImGui_ImplOpenGL3_Init();

	ImGui::StyleColorsDark();

	glfwSetKeyCallback(window, key_callback);
	glfwSetMouseButtonCallback(window, mouse_button_callback);
	glfwSetCursorPosCallback(window, mouse_pos_callback);
	glfwSetScrollCallback(window, scroll_callback);

	// UI flags
	bool showMetricsWindow = false;
	bool showPlanetWireframe = false;
	bool showPatchNormals = false;

	PlanetPatchBuffer patchBuffer;
	if(!patchBuffer.init())
		return false;

	std::vector<std::unique_ptr<Planet>> planets;
	planets.emplace_back(std::make_unique<Planet>(patchBuffer, glm::vec3(0, 0, 0), 400000));
	planets.emplace_back(std::make_unique<Planet>(patchBuffer, glm::vec3(4000000, 0, 4000000), 100000));
	for(auto& planet : planets)
	{
		if (!planet->init())
		{
			std::cin.get();
			return -1;
		}
	}
	
	Camera camera;
	camera.setFarZ(20000000);
	camera.transform().translate(0, 0, 500000.0f);

	CameraController cameraController(window, camera);
	gCameraController = &cameraController;

	double lastTime = glfwGetTime();
	while (!glfwWindowShouldClose(window))
	{
		// Poll and handle events (inputs, window resize, etc.)
		// You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
		// - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
		// - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
		// Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
		glfwPollEvents();

		// Sort planets from far away from camera to closest
		std::sort(planets.begin(), planets.end(),
			[&camera](const std::unique_ptr<Planet>& p1, const std::unique_ptr<Planet>& p2) -> bool
		{
			double d1 = glm::length(camera.transform().translation() - p1->transform().translation()) - p1->radius();
			double d2 = glm::length(camera.transform().translation() - p2->transform().translation()) - p2->radius();
			return d1 > d2;
		});

		const Planet& closestPlanet = *planets.back();

		double planetDistance = glm::length(camera.transform().translation() - closestPlanet.transform().translation()) - closestPlanet.radius();

		double currentTime = glfwGetTime();
		float dt = static_cast<float>(currentTime - lastTime);
		lastTime = currentTime;
		cameraController.update(dt, closestPlanet);

		int displayWidth, displayHeight;
		glfwMakeContextCurrent(window);

		glfwGetFramebufferSize(window, &displayWidth, &displayHeight);
		glViewport(0, 0, displayWidth, displayHeight);
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClearStencil(0);
		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);

		Frustum frustum = camera.buildFrustum(static_cast<float>(displayWidth)/static_cast<float>(displayHeight));

		if(showPlanetWireframe) glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );

		PlanetRenderStats planetRenderStats;

//		// Rotate sjoer planet around bas bol
//		planets[1]->transform().setTranslation(std::sin(currentTime/(60*5))*20000, 0, std::cos(currentTime/(60*50))*20000);

		for(const auto& planet : planets)
		{
			glClear(GL_STENCIL_BUFFER_BIT);
			planet->drawTerrain(frustum, planetRenderStats);
			planet->drawAtmosphere(frustum, planetRenderStats);
		}

		// Restore state
		if(showPlanetWireframe)	glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );

		// Get DPI setting

		// Start the ImGui frame
		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();

		if(ImGui::BeginMainMenuBar())
		{
			if (ImGui::BeginMenu("File"))
			{
				ImGui::MenuItem("Metrics", nullptr, &showMetricsWindow);
				
				ImGui::Separator();

				if(ImGui::MenuItem("Quit", "Alt+F4"))
					glfwSetWindowShouldClose(window, true);
				ImGui::EndMenu();
			}

			if(ImGui::BeginMenu("Planet"))
			{
				ImGui::Checkbox("Wireframe", &showPlanetWireframe);
				ImGui::SliderFloat("Rayleigh", &planets[1]->atmosphere().Kr, 0.0f, 0.01f, "%.4f");
				ImGui::SliderFloat("Mie", &planets[1]->atmosphere().Km, 0.0f, 0.01f, "%.4f");
				ImGui::SliderFloat("ESun", &planets[1]->atmosphere().ESun, 1.0f, 50.f, "%.1f");
				ImGui::SliderFloat3("Wavelength", &planets[1]->atmosphere().waveLength[0], 0.0f, 1.f, "%.3f");
				ImGui::SliderFloat("Size", &planets[1]->atmosphere().height, 2000.0f, 500000.f, "%.0f");
				ImGui::SliderFloat("ScaleDepth", &planets[1]->atmosphere().scaleDepth, 0,1, "%.3f");

				ImGui::MenuItem("Patch normals", nullptr, &showPatchNormals);

				ImGui::EndMenu();
			}

			ImGui::EndMainMenuBar();
		}

		/*if(showMetricsWindow)
		{

			ImGui::ShowMetricsWindow(&showMetricsWindow);
		}*/

		//ImGui::ShowMetricsWindow(&showMetricsWindow);
		if(showMetricsWindow)
		{
			ImGui::Begin("Metrics", &showMetricsWindow, ImGuiWindowFlags_AlwaysAutoResize|ImGuiWindowFlags_NoCollapse);
			ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
			ImGui::Text("Draw calls: %u", planetRenderStats.drawCalls);
			ImGui::Text("Triangles: %u", planetRenderStats.triangles);
			ImGui::Text("Movement speed: %.0f m/s", cameraController.movementSpeed());
			ImGui::Text("Altitude: %.0f m", planetDistance);
			
			ImGui::End();
		}

		if(showPatchNormals)
		{
			ImGui::Begin("PatchNormals", &showPatchNormals, ImGuiWindowFlags_NoCollapse);
			ImGui::Text("Resident patches: %u", static_cast<unsigned>(patchBuffer.residentPatchCount()));
			const float size = ImGui::GetContentRegionAvailWidth();
			ImGui::Image(reinterpret_cast<void*>(static_cast<uintptr_t>(*patchBuffer.normal_texture_atlas().texture())), ImVec2(size,size) );
			ImGui::End();
		}

		// Rendering
		ImGui::Render();

		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

		glfwMakeContextCurrent(window);
		glfwSwapBuffers(window);
	}

	planets.clear();

	glfwDestroyWindow(window);
	glfwTerminate();
	return 0;
}
