#include "streaming_buffer.h"

//-------------------------------------------------------------------------------------------------
StreamingBuffer::StreamingBuffer(int target, size_t byteSize) : target_(target), byteSize_(byteSize)
{
	
}

//-------------------------------------------------------------------------------------------------
bool StreamingBuffer::init()
{
	buffer_ = GLBufferPtr::Create();
	glBindBuffer(target_, *buffer_);
	glBufferData(target_, byteSize_, nullptr, GL_DYNAMIC_DRAW);
	glBindBuffer(target_, 0);
	return buffer_;
}

//-------------------------------------------------------------------------------------------------
void* StreamingBuffer::map(size_t size, size_t *bufferOffset)
{
	assert(size <= byteSize_);

	GLbitfield flags = GL_MAP_UNSYNCHRONIZED_BIT|GL_MAP_INVALIDATE_RANGE_BIT;
	if(currentOffset_ + size >= byteSize_)
	{
		currentOffset_ = 0;
		flags = GL_MAP_INVALIDATE_BUFFER_BIT;
	}

	glBindBuffer(target_, *buffer_);
	void * result = glMapBufferRange(target_, currentOffset_, size, GL_MAP_WRITE_BIT |flags);
	assert(result != nullptr);
	glBindBuffer(target_, 0);

	if(bufferOffset)
		*bufferOffset = currentOffset_;
	currentOffset_ += size;
	
	return result;
}

//-------------------------------------------------------------------------------------------------
void StreamingBuffer::unmap()
{
	glBindBuffer(target_, *buffer_);
	glUnmapBuffer(target_);
	glBindBuffer(target_, 0);
}

//-------------------------------------------------------------------------------------------------
const GLBufferPtr& StreamingBuffer::buffer() const
{
	return buffer_;
}
