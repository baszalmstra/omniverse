#pragma once

#include <common/gl_resource.h>
#include <cstdint>

class StreamingBuffer
{
public:
	StreamingBuffer(int target, size_t maxByteSize);

	bool init();

	void* map(size_t size, size_t *bufferOffset = nullptr);
	void unmap();

	const GLBufferPtr& buffer() const;

private:
	int target_;
	size_t byteSize_, currentOffset_ = 0;
	GLBufferPtr buffer_;
};