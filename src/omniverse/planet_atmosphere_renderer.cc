#include "planet_atmosphere_renderer.h"
#include "mesh/ico_sphere.h"
#include <common/shaders.h>
#include "transform.h"
#include "camera.h"
#include "planet.h"

//-------------------------------------------------------------------------------------------------
PlanetAtmosphereRenderer::PlanetAtmosphereRenderer(const Planet& planet) :
	planet_(planet)
{
	
}

//-------------------------------------------------------------------------------------------------
bool PlanetAtmosphereRenderer::init()
{
	mesh_ = std::make_unique<MeshGeometry>(mesh::generate_ico_sphere(4));

	if(program_ = shaders::program_from_files("shaders/sky.vert", "shaders/sky.frag"))
	{
		uniformViewProjection_ = glGetUniformLocation(*program_, "ViewProjection");
    atmosphereUniforms_ = atmosphere::shader::find_uniforms(program_);
	}
	else
		return false;

	return true;
}

//-------------------------------------------------------------------------------------------------
void PlanetAtmosphereRenderer::draw(const Frustum& frustum, PlanetRenderStats& stats)
{
	Transform cameraPlanet = planet_.transform().inverse() * frustum.transform();
	Frustum frustumPlanet(cameraPlanet, frustum.projection());

	const glm::mat4 projection = frustumPlanet.projection();
	const glm::mat4 view = frustumPlanet.view();
	const glm::mat4 viewProjection = projection*view;
	const glm::vec3 sunDir(1, 0, 0);

  glDisable(GL_DEPTH_TEST);
  
	// Setup stencil masking to only render the sky where there is no terrain
	glEnable(GL_STENCIL_TEST);
	glStencilFunc(GL_NOTEQUAL, 1, 0x1);
	glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
	glStencilMask(0x1);

	// Enable alpha blending
	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE);

	// Enable backface culling
	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);

  glUseProgram(*program_);
  glUniformMatrix4fv(uniformViewProjection_, 1, GL_FALSE, &viewProjection[0][0]);
  atmosphere::shader::set_uniforms(atmosphereUniforms_, planet_.atmosphere(),
    static_cast<float>(planet_.radius()),
    cameraPlanet.translation(),
    sunDir);

	mesh::draw(*mesh_);
	stats.drawCalls++;

	glDisable(GL_STENCIL_TEST);
	glDisable(GL_BLEND);
	glDisable(GL_CULL_FACE);
}
