#pragma once

#include <common/gl_resource.h>
#include <memory>
#include <string_view>

#include <glm/mat4x4.hpp>

#include "transform.h"
#include "camera.h"
#include <vector>
#include "planet_quad_tree.h"
#include "streaming_buffer.h"

#include "atmosphere/parameters.h"
#include "atmosphere/shader.h"

struct PlanetRenderStats
{
	uint32_t drawCalls = 0;
	uint32_t triangles = 0;
};

class Planet
{
public:
	Planet(class PlanetPatchBuffer &patchBuffer, const glm::vec3& pos, double radius);
	~Planet();
	void createIndexBuffer();

	Transform &transform() { return transform_;}

	const Transform &transform() const { return transform_;}

	bool init();

	void drawTerrain(const class Frustum& frustum, PlanetRenderStats& stats);
	void drawAtmosphere(const class Frustum& frustum, PlanetRenderStats& stats);

	constexpr double radius() const { return radius_; }

	class PlanetPatchBuffer& patchBuffer() const { return patchBuffer_; }

	const atmosphere::Parameters &atmosphere() const { return atmosphere_; };
	atmosphere::Parameters &atmosphere() { return atmosphere_; };

private:
	atmosphere::Parameters atmosphere_;
	std::unique_ptr<class PlanetAtmosphereRenderer> atmosphereRenderer_;
	
	class PlanetPatchBuffer &patchBuffer_;

	double radius_;
	std::string heightMapPath_;

	Transform transform_;

	std::unique_ptr<std::array<PlanetQuadTree, 6>> faces_;

	std::unique_ptr<class PlanetGenerator> generator_;
	
	std::unique_ptr<class PatchGenerationProcessor> processor_;

	GLTexturePtr detailMap_;

	GLProgramPtr program_;
	int uniformViewProjection_;
	int uniformNormalMapTexelSize_; 
	int uniformHeightMapTexelSize_;
	int uniformAtlasPatchSize_;
	int uniformHeightMap_, uniformNormalMap_, uniformDetailMap_;
	int uniformPatchGridSize_;
	int uniformMorphFactor_;
  atmosphere::shader::Uniforms atmosphereUniforms_;
  	
	GLVertexArrayPtr vao_;
	GLBufferPtr nodeIndexBuffer_;
	GLBufferPtr instanceIndexBuffer_;

	StreamingBuffer patchTransformationBuffer_;
};
