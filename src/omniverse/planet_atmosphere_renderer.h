#pragma once

#include <memory>
#include "common/gl_resource.h"
#include "atmosphere/shader.h"

class PlanetAtmosphereRenderer
{
public:
	explicit PlanetAtmosphereRenderer(const class Planet& planet);

	bool init();
	
	void draw(const class Frustum& frustum, struct PlanetRenderStats& stats);

private:
	const Planet& planet_;

	std::unique_ptr<struct MeshGeometry> mesh_;

	GLProgramPtr program_;
	int uniformViewProjection_;
  atmosphere::shader::Uniforms atmosphereUniforms_;
};
