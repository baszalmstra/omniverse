#include "planet_quad_tree.h"
#include "planet.h"
#include "planet_generator.h"
#include "planet_patch_buffer.h"
#include <vector>
#include <algorithm>

const double pi = 3.1415926535897932384626433;

//-------------------------------------------------------------------------------------------------
PlanetQuadTreeNode::~PlanetQuadTreeNode()
{
	cancelGeneration();
}

//-------------------------------------------------------------------------------------------------
void PlanetQuadTreeNode::cancelGeneration()
{
	std::shared_ptr<std::atomic_bool> cancellationToken;
	generationCancellationToken_.swap(cancellationToken);
	if(cancellationToken)
		cancellationToken->store(true);
}

//-------------------------------------------------------------------------------------------------
PlanetQuadTree::PlanetQuadTree(const Planet &planet, PatchGenerationProcessor& processor, PlanetFace face) :
	planet_(planet),
	processor_(processor),
	face_(face)
{
	const uint32_t lodOffset = 1;
  const double maximumQuadSize = 0.5 * pi * planet_.radius();
	maxLODLevel_ = static_cast<uint32_t>(std::ceil(std::log2(maximumQuadSize)) - lodOffset);

	initializeLeaf(root_, glm::dvec2(0,0), 1.0, 0);

	uint32_t maxLodLevel = maxLODLevel();
	splitDistances_.reserve(maxLodLevel);
	splitDistances_.push_back(2);
  for(uint32_t i = 0; i < maxLodLevel; ++i)
		splitDistances_.push_back(splitDistances_.back() * 2);
}

//-------------------------------------------------------------------------------------------------
void PlanetQuadTree::queryVisibleNodes(const Frustum& frustumPlanet, std::deque<VisibleNode>& visibleNodes)
{
	const uint32_t maxLodLevel = maxLODLevel();
	horizon_culling::Cone<double> cone(frustumPlanet.transform().translation(), planet_.radius());
	
	if(root_.has_geometry())
		queryVisibleNodes(frustumPlanet, cone, visibleNodes, root_, glm::dvec2(0,0), 1.0, maxLodLevel, false);
}

//-------------------------------------------------------------------------------------------------
void PlanetQuadTree::ensurePatchesResident(const Frustum& frustumPlanet)
{
	const uint32_t maxLodLevel = maxLODLevel();
	ensurePatchesResident(frustumPlanet, root_, glm::dvec2(0,0), 1.0, maxLodLevel);
}

//-------------------------------------------------------------------------------------------------
void PlanetQuadTree::initializeLeaf(PlanetQuadTreeNode & node, const glm::dvec2 &topLeftUv, double uvSize, uint32_t lodLevel)
{
	const uint32_t VertexCount = PlanetQuadTreeNode::VerticesPerPatch*PlanetQuadTreeNode::VerticesPerPatch;
	const uint32_t NormalCount = PlanetQuadTreeNode::NormalsPerPatch*PlanetQuadTreeNode::NormalsPerPatch;

  const double overlapDistance = 2.0 / static_cast<float>(PlanetQuadTreeNode::VerticesPerPatch - 4);

  PlanetPatch patch {}; 
  patch.face = face_;
  patch.sizeUv = uvSize*(1.0f+2.0f*overlapDistance);
  patch.topLeftUv = topLeftUv-glm::dvec2(uvSize*overlapDistance);

//	planetPositions.resize(VertexCount);

	// Create a request for generating a node
	std::shared_ptr<GenerationInput> generationRequest(new GenerationInput{&node, patch, lodLevel});
	std::shared_ptr<std::atomic_bool> cancellationToken(generationRequest, &generationRequest->cancellationToken);
	cancellationToken.swap(node.generationCancellationToken_);
	if(cancellationToken)
		cancellationToken->store(true);

	generationRequest->planetPositions.resize(VertexCount);
	generationRequest->planetNormals.resize(NormalCount);	

	processor_.queue(generationRequest);
}

//-------------------------------------------------------------------------------------------------
void PlanetQuadTree::processGenerationRequest() const 
{
	// Queue in the processor
	std::deque<GenerationInputPtr> results(processor_.retrieveResults());
	
	for(GenerationInputPtr& ptr : results)
	{
		if(ptr->cancellationToken)
			continue;

		auto &node = *ptr->node;

		const uint32_t VerticesPerPatch = PlanetQuadTreeNode::VerticesPerPatch;
		const double VertexStep = ptr->patch.sizeUv / (VerticesPerPatch - 1);

		node.posPlanet = ptr->planetPositions[0];

		// Calculate AABB
		AABB aabb;
		for (const auto &p : ptr->planetPositions)
			aabb = aabb.united(p);

		node.patchId = planet_.patchBuffer().acquire();
		assert(node.patchId.has_value());

    std::vector<float> heights(ptr->planetPositions.size());
    
		// Transform all positions to patch space
		PlanetVertex *vertices = planet_.patchBuffer().geometry_buffer().map(*node.patchId);
		double uvScale = pi * 0.5 * planet_.radius();
		double uvStep = VertexStep * uvScale;

		glm::dvec3 tangent = glm::normalize(ptr->planetPositions[VerticesPerPatch-1] - ptr->planetPositions[0]);
		glm::dvec3 binormal = glm::normalize(ptr->planetPositions[(VerticesPerPatch-1)*VerticesPerPatch] - ptr->planetPositions[0]);
		glm::dvec3 normal = glm::normalize(glm::cross(tangent, binormal));
		glm::dvec3 realBinormal = glm::normalize(glm::cross(normal, tangent));
		glm::dmat3 rotationMatrix = glm::dmat3 { tangent, realBinormal, normal };
		glm::dmat3 invRotationMatrix = glm::transpose(rotationMatrix);
		node.localToPlanet = rotationMatrix;

		glm::dvec2 uvStart(fmod(ptr->patch.topLeftUv.x * uvScale, 1.0), fmod(ptr->patch.topLeftUv.y * uvScale, 1.0));
		for (std::size_t y = 0; y < VerticesPerPatch; ++y) {
			for (std::size_t x = 0; x < VerticesPerPatch; ++x) {
				const auto index = y * VerticesPerPatch + x;
        glm::dvec3 relativePos = ptr->planetPositions[index] - node.posPlanet;
				glm::dvec3 localPos = invRotationMatrix*relativePos;
				
				// Compute the vertex index that this vertex will morph to while morphing
				size_t morphTargetIndex = index - 
					((x % 2) * 1) -
					((y % 2) * VerticesPerPatch);
				
        vertices[index].posAndMorphDelta.x = static_cast<float>(localPos.x);
				vertices[index].posAndMorphDelta.y = static_cast<float>(localPos.y);
				vertices[index].posAndMorphDelta.z = vertices[morphTargetIndex].posAndMorphDelta.x;
				vertices[index].posAndMorphDelta.w = vertices[morphTargetIndex].posAndMorphDelta.y;

				heights[index] = static_cast<float>(localPos.z);
				vertices[index].localTexCoords.x = static_cast<float>(x)/(VerticesPerPatch-1); 
				vertices[index].localTexCoords.y = static_cast<float>(y)/(VerticesPerPatch-1);

        vertices[index].texcoord.x = static_cast<float>(uvStart.x + x * uvStep);
        vertices[index].texcoord.y = static_cast<float>(uvStart.y + y * uvStep);
        vertices[index].texcoord.z = vertices[morphTargetIndex].texcoord.x;
        vertices[index].texcoord.w = vertices[morphTargetIndex].texcoord.y;
			}
		}

		planet_.patchBuffer().geometry_buffer().unmap(*node.patchId);

    planet_.patchBuffer().normal_texture_atlas().write(*node.patchId, 0,
      av::array_view<glm::vec3>(ptr->planetNormals));
    
    /*uint32_t normalsMipLevelSize = (PlanetQuadTreeNode::NormalsPerPatch >> 1);
    std::vector<glm::vec3> normalsNextLevel(normalsMipLevelSize*normalsMipLevelSize);
    glm::vec3 *normalMipMapData = normalsNextLevel.data();
    glm::vec3 *normalData = ptr->planetNormals.data();
    for (std::size_t y = 0; y < normalsMipLevelSize; ++y)
    {
      for (std::size_t x = 0; x < normalsMipLevelSize; ++x)
      {
        normalMipMapData[y*normalsMipLevelSize + x] = normalData[(y * 2)*PlanetQuadTreeNode::NormalsPerPatch + (x * 2)];
      }
    }

    planet_.patchBuffer().normal_texture_atlas().write(*node.patchId, 1,
      av::array_view<glm::vec3>(normalsNextLevel));*/

    planet_.patchBuffer().height_offset_texture_atlas().write(*node.patchId, 0,
      av::array_view<float>(heights));

		// Store the buffer in the quad tree node
		//node.vertexBuffer = std::move(vertexBuffer);
		node.aabb = aabb;
	}
}

namespace {
	//-------------------------------------------------------------------------------------------------
	void addNodeToVisibleList(const Frustum& frustumPlanet, PlanetQuadTreeNode& node, uint32_t nodeDepth,
		const std::vector<double>& splitDistances,
		std::deque<VisibleNode>& visibleNodes, 
		VisibleNode::NodePart part = VisibleNode::Whole)
	{
		VisibleNode visibleNode{};
	  visibleNode.node = &node;
		glm::mat4 mat = { node.localToPlanet };
		visibleNode.poseCamera = Transform(node.posPlanet - frustumPlanet.transform().translation()).transformation() * mat;
		visibleNode.part = part;
		
    double currentSplitDepth = nodeDepth > 0 ? splitDistances[nodeDepth - 1] : 0;
    double previousSplitDepth = splitDistances[nodeDepth];
    double splitDepth = currentSplitDepth + (previousSplitDepth - currentSplitDepth)*0.80f;
    visibleNode.morphRange = glm::vec2(splitDepth, previousSplitDepth);

	  visibleNodes.emplace_back(visibleNode);
	}
}

//-------------------------------------------------------------------------------------------------
PlanetQuadTree::LODSelectResult PlanetQuadTree::queryVisibleNodes(
	const Frustum& frustumPlanet, const horizon_culling::Cone<double> &cone,
	std::deque<VisibleNode>& visibleNodes,
	PlanetQuadTreeNode &node, const glm::dvec2& topLeftUv, double sizeUv, uint32_t depth, 
	bool parentCompletelyInFrustum)
{
	assert(node.has_geometry());

	const auto frustumContainment = parentCompletelyInFrustum ? Containment::Inside : frustumPlanet.classify(node.aabb);
	if(frustumContainment == Containment::Outside)
	{
		// The node does not lie inside the frustum, select nothing but return true to mark this node as
		// having been correctly handled so that the parent node does not select itself over our area.
		return LODSelectResult::OutOfFrustum;
	}

	if(cone.isInside(node.aabb))
	{
		// The node was culled by the horizon of the planet
		return LODSelectResult::OutOfFrustum;
	}

	if(depth == 0)
	{
		// The last lod level is just always added to the selection list
		addNodeToVisibleList(frustumPlanet, node, depth, splitDistances_, visibleNodes);
		return LODSelectResult::Selected;
	}

	// Check if the node is within it's correct LOD level
	const auto cameraPos = frustumPlanet.transform().translation();
	if(!node.aabb.intersectsSphere(cameraPos, splitDistances_[depth]))
	{
		// No matter what, the highest lod level is always selected 
		if(depth == maxLODLevel())
		{
			addNodeToVisibleList(frustumPlanet, node, depth, splitDistances_, visibleNodes);
			return LODSelectResult::Selected;	
		}

		return LODSelectResult::OutOfRange;
	}
	
	// If the node has no more detailed LOD levels the lower detailed node is selected.
	const bool hasChildren = node.children != nullptr;
	if(!hasChildren)
	{
		addNodeToVisibleList(frustumPlanet, node, depth, splitDistances_, visibleNodes);
		return LODSelectResult::Selected;
	}

	// Cover the more detailed LOD level range: some or all of the four children nodes will have to be
	// selected instead.
	std::array<LODSelectResult, 4> subSelectionResults {LODSelectResult::Undefined};
	/*std::array<std::pair<int, double>, 4> subSectionDistance;
	for(int i = 0; i < 4; ++i)
	{
		auto &childNode = node.children->at(i);
		if(childNode.has_geometry())
			subSectionDistance[i] = std::make_pair(i, childNode.aabb.distanceSquared(cameraPos));
		else
			subSectionDistance[i] = std::make_pair(i, std::numeric_limits<double>::max());
	}

	std::sort(subSectionDistance.begin(), subSectionDistance.end(), [](const auto &a, const auto& b) { return a.second < b.second; });*/

	const bool nodeCompletelyInFrustum = frustumContainment == Containment::Inside;
	//for(const auto& section : subSectionDistance)
	for(int index = 0; index < 4; ++index)
	{
		//const uint32_t index = section.first;
		int x = index % 2;
		int y = index / 2;
		auto &childNode = node.children->at(index);
		
		if(childNode.has_geometry())
      subSelectionResults[index] = queryVisibleNodes(frustumPlanet, cone,
				visibleNodes, 
				childNode,
				topLeftUv + glm::dvec2(sizeUv*0.5*x, sizeUv*0.5*y),
				sizeUv*0.5,
				depth - 1,
				nodeCompletelyInFrustum);
	}

	const auto isNotSelected = [](auto selection) {
	  return selection == LODSelectResult::Undefined || selection == LODSelectResult::OutOfRange;
  };

	// If none of the nodes was selected because they either lack geometry or where out of range we'll have
	// to fill in the geometry with lower detail geometry from this node.
	if(std::all_of(subSelectionResults.begin(), subSelectionResults.end(), isNotSelected))
  {
		addNodeToVisibleList(frustumPlanet, node, depth, splitDistances_, visibleNodes);
		return LODSelectResult::Selected;
  }

	// If any of the nodes is not select because it has no geometry or because it's out of range we'll
	// fill it in with lower details from this node.
	for(auto i = 0; i < 4; i++)
		if(isNotSelected(subSelectionResults[i]))
			addNodeToVisibleList(frustumPlanet, node, depth, splitDistances_, visibleNodes, static_cast<VisibleNode::NodePart>(i));	

	return std::any_of(subSelectionResults.begin(), subSelectionResults.end(), [](auto selection) { return selection == LODSelectResult::Selected; }) ? 
		LODSelectResult::Selected : 
		LODSelectResult::OutOfFrustum;
}

//-------------------------------------------------------------------------------------------------
void PlanetQuadTree::ensurePatchesResident(const Frustum& frustumPlanet, PlanetQuadTreeNode& node,
	const glm::dvec2& topLeftUv, double sizeUv, uint32_t depth)
{
	if(depth == 0)
		return;

	const auto cameraPos = frustumPlanet.transform().translation();
	if(!node.aabb.intersectsSphere(cameraPos, splitDistances_[depth]))
	{
		merge(node);
		return;
	}

	// If the node has no more detailed LOD levels the lower detailed node is selected.
	const bool hasChildren = node.children != nullptr;
	if(!hasChildren)
	{
		node.children = std::make_unique<std::array<PlanetQuadTreeNode, 4>>();
		for (int y = 0; y < 2; y++)
			for (int x = 0; x < 2; x++)
			{
				initializeLeaf(node.children->data()[y*2 + x],
					topLeftUv + glm::dvec2(sizeUv*0.5*x, sizeUv*0.5*y),
					sizeUv*0.5, maxLODLevel_-(depth-1));
			}
	}

	// Cover the more detailed LOD level range: some or all of the four children nodes will have to be
	// selected instead.
	for (int y = 0; y < 2; y++)
    for (int x = 0; x < 2; x++)
      ensurePatchesResident(frustumPlanet, node.children->at(y * 2 + x),
				topLeftUv + glm::dvec2(sizeUv*0.5*x, sizeUv*0.5*y),
				sizeUv*0.5,
				depth - 1);
}

//-------------------------------------------------------------------------------------------------
void PlanetQuadTree::merge(PlanetQuadTreeNode& node)
{
	if(node.has_children())
	{
		for(auto &child : *node.children)
		{
			destroyLeaf(child);
		}
		node.children.reset();
	}
}

//-------------------------------------------------------------------------------------------------
void PlanetQuadTree::destroyLeaf(PlanetQuadTreeNode& node)
{
	if(node.is_initialized())
	{
		merge(node);
		planet_.patchBuffer().release(*node.patchId);
	}
}
